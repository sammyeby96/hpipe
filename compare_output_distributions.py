import numpy as np
import seaborn as sns
import sys
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure

def get_oc_labels(arr):
	indices = np.arange(arr.shape[0])
	total_indices = []
	for i in indices:
		total_indices.extend([i] * arr[i].size)
	return np.array(total_indices)

def get_generator_labels(arr, name):
	return np.repeat(name, np.prod(arr.shape))

def get_violinplot_data(arr_dict):
	first_values = list(arr_dict.values())[0]
	data_type = type(first_values)
	while data_type in [object, list, np.ndarray]:
		first_values = first_values[0]
		data_type = type(first_values)
	data = {
		"Generator": np.empty([0], dtype=np.dtype("<U5")),
		"Output Channel": np.empty([0], dtype=np.dtype("int64")),
		"Values": np.empty([0], dtype=np.dtype(type(list(arr_dict.values())[0].flat[0])))}
	for k,v in arr_dict.items():
		if np.array([len(v[0]) != len(vn) for vn in v[1:]]).any():
			fv = np.concatenate(list(v.flat))
		else:
			fv = v.flatten()
		data["Values"] = np.concatenate([data["Values"], fv]).astype(data_type)
		data["Output Channel"] = np.concatenate([
			data["Output Channel"],
			get_oc_labels(v)])
		data["Generator"] = np.concatenate([
			data["Generator"], 
			get_generator_labels(fv, k)])
	return data

def check_args():
	if len(sys.argv) < 3:
		print("Usage:")
		print("  " + sys.argv[0] + " <accelerator_accum_output_path> <tensorflow_accum_output_path> [output_path]")
		print("  E.g. python compare_output_distributions.py /home/mat/courses/first_year/second_semester/arch/scnn/tests/l1_accum_out.npy conv_scnn_output/activations/1.npy")
		sys.exit(0)

def load_tf_acts(path):
	tf = np.load(path)
	tf = tf[0]
	tf = tf.transpose([2,0,1])
	return tf

def main():
	check_args()
	a = np.load(sys.argv[1])
	tf = load_tf_acts(sys.argv[2])
	output_channels = a.shape[0]
	vplot_data = get_violinplot_data({
		"Accelerator": a,
		"TensorFlow": tf})
	ax = sns.violinplot(data=vplot_data, hue="Generator", 
		x="Output Channel", y="Values", split=False, inner="quartile")
	if len(sys.argv) > 3:
		fig = plt.gcf()
		fig.set_size_inches(60*output_channels/64,10)
		fig.savefig(sys.argv[3], bbox_inches="tight")
	else:
		plt.show()


if __name__ == "__main__":
    main()

