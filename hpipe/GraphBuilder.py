import numpy as np
import math
from digitallogic.utils import get_nodes, get_tensor_to_producer_op_map, get_tensor_to_consumer_op_map, get_quant_op, get_quantized, clog2
import tensorflow
try:
	import tensorflow.compat.v1 as tf
except:
	import tensorflow as tf
import os
import sys
from collections import namedtuple

# Set this to a number greater than zero to enable int saturation for lower quantizations (10-bits or less)
# This will sacrafice an integer bit for more fractional bits if the error decreases (i.e. only a couple of numbers will saturate)
QUANT_SATURATION_FACTOR = 2
# Percentage of values that should be allowed to saturate. Currently this is a bit arbitrary as it's had to test without doing a ton of compiles
QUANT_SATURATION_THRESHOLD = 0.05

# Requires psutil
# This will print the current memory in GB used by the program
# Useful for debugging high memory usage
def print_mem_use():
	import psutil
	pid = os.getpid()
	py = psutil.Process(pid)
	memoryUse = py.memory_info()[0]/2.**30  
	print('Memory use:', memoryUse, "GB")

def get_precision_spec_from_min_max(_min, _max, bits):
	if bits % 2 != 0:
		bits += 1
		
	max_mag = max(abs(_min), _max)
	if max_mag == 0:
		print("ERROR: min and max for the quantize node should not be 0")
		raise ValueError


	#Extrapolate int bits 
	int_bits = clog2(max_mag)
	
	#This is probably not needed, we can have 0 int bits
	#if int_bits < 1:
	#	int_bits = 1
		
	if int_bits > 6:
		int_bits = 6
	sign_bits = 1
	frac_bits = bits - (int_bits + sign_bits)
	
	#print("	Quantization (sign, int, frac):", sign_bits, int_bits, frac_bits)
		
	assert(frac_bits >= 0)
	return {
		"exponent" : 0,
		"a_exponent" : 0,
		"a_int" : int_bits,
		"a_f" : frac_bits,
		"a_sign_bits" : sign_bits,
		"a_width" : int_bits + frac_bits + sign_bits,
		"a_min" : _min,
		"a_max" : _max
	}


class Node():
	def __init__(self, tf_op, example_outputs, precision_parameters=None):
		self.tf_op = tf_op
		self.properties = {}
		self.values = []
		self.inputs = []
		self.outputs = []
		self.type = tf_op.type
		self.example_outputs = example_outputs
		self.planner_estimate = None
		self.ocs_pruned = []
		self.ic_skipped_lists = []
		self.explicit_padding = None
		self.precision_parameters = precision_parameters
		self.module = None
		self.example_input = None


		properties_to_gather = [
			"padding",
			"strides",
			"dilation_rate",
			"data_format",
			"ksize",
			"axis"]
		
		if hasattr(tf_op, "get_attr"):
			for p in properties_to_gather:
				for p in properties_to_gather:
					try:
						prop = tf_op.get_attr(p)
						self.properties[p] = prop
						if p == "data_format":
							prop = prop.decode("utf-8")
							assert(prop.lower() == "nhwc")
					except ValueError:
						continue


	def input_nodes(self):
		return [e._from for e in self.inputs]

	def output_nodes(self):
		return [e._to for e in self.outputs]

	def set_type(self, _type):
		self.type = _type
	
	def get_type(self):
		return self.type

	def add_input(self, _input):
		self.inputs.append(_input)

	def set_inputs(self, inputs):
		self.inputs = inputs
		
	# If input index is not specified will return an error if inputs size is greater than 1
	def get_input(self, input_index=None):
		if len(self.inputs) == 0: 
			print("WARNING: The node you used get_input on has no inputs")
			return None
		
		if input_index == None:
			if len(self.inputs) > 1: 
				print("WARNING: Node has more than one input and you have not specified an input index in get_input")
				return None
			else:
				input_index = 0
		
		return self.inputs[input_index]._from

	def add_output(self, output):
		self.outputs.append(output)

	def set_outputs(self, outputs):
		self.outputs = outputs

	def get_output_shape(self):
		return self.example_outputs[0].shape

	def _is_supported_filter(self):
		return (self.type in ["Conv2D", "DepthwiseConv2dNative", "MaxPool", "AvgPool"])

	def get_kernel_shape(self):
		assert(self._is_supported_filter())
		if self.type in ["MaxPool", "AvgPool"]:
			print(self.properties)
			return [self.properties["ksize"][i] for i in [1,2,0,3]]
		if len(self.values[0].shape) != 4:
			print("weights for node " + self.tf_op.name + " must have 4 dimensions, but has shape " + str(self.values[0].shape))
			assert(False)
		return self.values[0].shape

	def get_strides(self):
		assert(self._is_supported_filter())
		assert(len(self.properties["strides"]) == 4)
		if self.type == "MaxPool":
			return [self.properties["strides"][i] for i in [1,2,0,3]]
		return self.properties["strides"]

	def get_bits(self, field="width", is_act=False):
		field_to_name = {
			"int"        : "int",
			"fractional" : "f",
			"sign"       : "sign_bits",
			"exponent"   : "exponent",
			"width"      : None
		}
		name = field_to_name[field]
		if name is None:
			keys = [v for v in field_to_name.values() if v is not None]
			if is_act:
				keys = ["a_" + k for k in keys]
			width = np.sum([self.precision_parameters[k] for k in keys])
			return width
		if is_act:
			name = "a_" + name
		if name in ["sign_bits", "a_sign_bits"]:
			return self.precision_parameters[name] == 1
		return self.precision_parameters[name]

	def requires_input_buffering(self):
		if self.type in ["Relu", "BiasAdd"]:
			return False
		return True

	def get_parallel_acts_per_memory(self, mem_width=40):
		width = self.get_bits("width", is_act=True)
		return mem_width // width



class Edge():
	def __init__(self, _from, _to):
		self.properties = {}
		self._from = _from
		self._to = _to

	def add_property(self, name, value):
		self.properties[name] = value

class Graph():
	def __init__(self):
		self.heads = []
		self.nodes = []
		self.edges = []
		self.internal_heads = []

	def walk_graph(self, f_node=None, f_edge=None, to_explore=None, up=False, depth_first=False):
		if to_explore == None:
			to_explore = self.heads.copy() + self.internal_heads.copy()
		explored = []
		while len(to_explore) > 0:
			current = to_explore.pop(0)
			explored.append(current)
			if f_node is not None:
				f_node(current)
			edges = current.outputs
			if up:
				edges = current.inputs
			for edge in edges:
				if f_edge is not None:
					f_edge(edge)
				if up:
					node = edge._from
				else:
					node = edge._to
				if node not in explored and node not in to_explore:
					if up or np.array([e._from in explored for e in node.inputs]).all():
						if depth_first:
							to_explore.insert(0, node)
						else:
							to_explore.append(node)


	def prune_from_output(self, output_names):
		to_explore = []
		for n in self.nodes:
			if n.tf_op.name in output_names:
				to_explore.append(n)
				n.outputs = []
		nodes = []
		edges = []
		self.walk_graph(
			lambda n: nodes.append(n),
			lambda e: edges.append(e),
			to_explore=to_explore, up=True)
		self.nodes = nodes
		self.edges = edges
		self.heads = [head for head in self.heads if head in nodes]
		self.internal_heads = [head for head in self.internal_heads if head in nodes]

		for n in self.nodes:
			to_del = []
			for i,o in enumerate(n.output_nodes()):
				if o not in self.nodes:
					to_del.append(i)
			for d in reversed(to_del):
				del n.outputs[d]


	def prune_dead_nodes(self):
		nodes = []
		edges = []
		self.walk_graph(
			lambda n: nodes.append(n),
			lambda e: edges.append(e))
		self.nodes = nodes
		self.edges = edges

	def print_graph_walk(self):
		self.walk_graph(
			lambda n: print("Node: " + n.tf_op.name + ", " + n.type))#,
			#lambda e: print("Edge: from: " + e._from.tf_op.name + ", to: " + e._to.tf_op.name + ", to_node_inputs: " + str([[ef._from.tf_op.name for ef in e._from.inputs] for e in e._to.inputs])))

	def get_output_nodes(self):
		output_nodes = []
		def f_node(n):
			if len(n.outputs) == 0:
				output_nodes.append(n)
		self.walk_graph(f_node)

	def convert_dense_to_conv2d(self):
		for n in self.nodes:
			if n.type not in ["Dense", "MatMul"] or (len(n.values) == 0):
				continue
			n.type = "Conv2D"
			n.values[0] = np.reshape(n.values[0], [1,1] + list(n.values[0].shape))
			n.properties["padding"] = "same"
			n.properties["strides"] = [1,1,1,1]
			for i in range(len(n.example_outputs)):
				n.example_outputs[i] = np.reshape(n.example_outputs[i], [1,1,1, n.example_outputs[i].size])
			parents = [e._from for e in n.inputs]
			while len(parents) > 0:
				parent = parents.pop(0)
				if len(parent.example_outputs[0].shape) < 4:
					parents += [e._from for e in parent.inputs]
				while len(parent.example_outputs[0].shape) < 4:
					parent.example_outputs[0] = parent.example_outputs[0][np.newaxis, ...]
				if parent.type == "Reshape":
					parent.values.insert(0, parent.example_outputs[0].shape)
			descendents = [e._to for e in n.outputs]
			while len(descendents) > 0:
				parent = descendents.pop(0)
				if len(parent.example_outputs[0].shape) < 4:
					descendents += [e._to for e in parent.outputs]
				while len(parent.example_outputs[0].shape) < 4:
					parent.example_outputs[0] = parent.example_outputs[0][np.newaxis, ...]

	def convert_add_constant_to_bias_add(self):
		for n in self.nodes:
			if (n.type != "Add" and n.type != "AddV2")  or (len(n.values) == 0):
				continue
			n.type = "BiasAdd"
			if n.values[0].shape[-1] != n.example_outputs[0].shape[-1]:
				n.values[0] = np.repeat(n.values, [1] * (len(n.values[0].shape) - 1) + [n.example_outputs[0].shape[-1]])

	def replace_avg_pool_with_mean(self):
		for n in self.nodes:
			if n.type != "AvgPool":
				continue
			input_shape = n.inputs[0]._from.get_output_shape()
			kernel_shape = n.get_kernel_shape()
			if input_shape[1] == kernel_shape[0] and input_shape[2] == kernel_shape[1]:
				n.type = "Mean"

	def cut_out_node(self, n):
		parents = [e._from for e in n.inputs]
		children = [e._to for e in n.outputs]
		for p in parents:
			p_indices_to_delete = []
			for i,e in enumerate(p.outputs):
				if e._to == n:
					p_indices_to_delete.append(i)
			p.outputs = list(np.delete(np.array(p.outputs), p_indices_to_delete))
			for c in children:
				c_indices_to_delete = []
				for i,e in enumerate(c.inputs):
					if e._from == n:
						c_indices_to_delete.append(i)
				c.inputs = list(np.delete(np.array(c.inputs), c_indices_to_delete))
				e = Edge(p, c)
				c.inputs.append(e)
				p.outputs.append(e)
				self.edges.append(e)

		if len(parents) == 0:
			for c in children:
				c_indices_to_delete = []
				for i,e in enumerate(c.inputs):
					if e._from == n:
						c_indices_to_delete.append(i)
				c.inputs = list(np.delete(np.array(c.inputs), c_indices_to_delete))


		for l in [self.heads, self.nodes, self.internal_heads]:
			if n in l:
				del l[l.index(n)]


	def merge_quant_ops(self):
		nodes_to_cut_out = []
		for n in self.nodes:	
			if "FakeQuant" in n.type:
				nodes_to_cut_out.append(n)
				_min = 0.0
				_max = 0.0
				input_node = None
				for e in n.inputs:
					e_node = e._from
					e_name = e_node.tf_op.name
					if "/min/" in e_name:
						_min = e_node.example_outputs[0]
					elif "/max/" in e_name:
						#print(e_name)
						_max = e_node.example_outputs[0]
					else:
						input_node = e_node
				#print("	", _min, _max)
				precision = get_precision_spec_from_min_max(_min, _max, n.tf_op.get_attr("num_bits"))
				input_node.example_outputs = n.example_outputs

				input_node.precision_parameters = precision

		for n in nodes_to_cut_out:
			self.cut_out_node(n)
			
		


	def cut_out_nodes_of_type(self, kind):
		nodes_to_cut_out = []
		for n in self.nodes:
			if n.type == kind:
				nodes_to_cut_out.append(n)
		for n in nodes_to_cut_out:
			self.cut_out_node(n)


	def push_pad_ops_into_convs(self):
		padding_to_add = {}
		def process_node(n):
			nonlocal padding_to_add
			node_has_explicit_padding = n.tf_op.name in padding_to_add.keys()
			def add_padding_to_children(padding):
				nonlocal padding_to_add
				nonlocal n
				for e in n.outputs:
					padding_to_add[e._to.tf_op.name] = padding
			if n.type == "Pad":
				padding = n.values[0]
				if node_has_explicit_padding:
					padding = np.array(padding) + np.array(padding_to_add[n.tf_op.name])
				add_padding_to_children(padding)
				self.cut_out_node(n)
			elif node_has_explicit_padding and n.type in ["Conv2D", "DepthwiseConv2D"]:
				n.explicit_padding = padding_to_add[n.tf_op.name]
			elif node_has_explicit_padding:
				add_padding_to_children(padding_to_add[n.tf_op.name])


		self.walk_graph(f_node=process_node)
		self.prune_dead_nodes()


	def prune_zero_ocs(self):
		def _prune_zero_ocs(n):
			if len(n.inputs) == 0:
				return
			if n.type in ["Conv2D", "DepthwiseConv2D"]:
				zero_ics = n.inputs[0]._from.ocs_pruned
				zero_ocs = np.nonzero((n.values[0] == 0).all(axis=(0,1,2)))[0]
				n.ocs_pruned = zero_ocs
				n.values[0] = np.delete(n.values[0], zero_ics, axis=2)
				n.values[0] = np.delete(n.values[0], zero_ocs, axis=3)
			elif n.type == "BiasAdd":
				zero_ics = n.inputs[0]._from.ocs_pruned
				zero_ocs = zero_ics
				n.ocs_pruned = zero_ocs
				assert((n.values[0][zero_ocs] <= 0).all())
				n.values[0] = np.delete(n.values[0], zero_ocs, axis=0)
			elif n.type in ["Add","AddV2"]:
				zero_ic_lists = [e._from.ocs_pruned for e in n.inputs]
				combined = zero_ic_lists[0]
				for zero_ics in zero_ic_lists[1:]:
					tmp = []
					for v in combined:
						if v in zero_ics:
							tmp.append(v)
					combined = tmp
				n.ic_skipped_lists = zero_ic_lists
				n.ocs_pruned = combined
			else:
				zero_ics = n.inputs[0]._from.ocs_pruned
				zero_ocs = zero_ics
				n.ocs_pruned = zero_ocs
			n.example_outputs[0] = np.delete(n.example_outputs[0], n.ocs_pruned, axis=3)

		self.walk_graph(f_node=_prune_zero_ocs)

	def find_constant_nodes(self):
		constants = []
		for n in self.nodes:
			if len(n.inputs) == 0 and n not in self.heads:
				constants.append(n)

		depends_on_placeholder = {}
		evaluated_constants = []
		def p_node(n):
			nonlocal depends_on_placeholder
			nonlocal evaluated_constants
			is_evaluated_constant = ((len(n.inputs) == 0 or
				np.all([depends_on_placeholder[i._from.tf_op.name] for i in n.inputs]))) and n.tf_op.type != "Placeholder"
			depends_on_placeholder[n.tf_op.name] = is_evaluated_constant
			if is_evaluated_constant:
				evaluated_constants.append(n)

		to_explore = self.heads.copy() + constants.copy()
		self.walk_graph(f_node=p_node, to_explore=to_explore)

		return evaluated_constants

	def merge_constants(self):
		constants = self.find_constant_nodes()
		
		#for n in constants:
		#	print(n.tf_op.name, " ", n.type)

		for n in self.nodes:
			for i in n.inputs:
				if i._from in constants:
					if i._from.example_outputs[0].size > 1:
						constant_output = i._from.example_outputs[0]
						#print(n.tf_op.name, constant_output)
						n.values.append(constant_output)
						if i._from.precision_parameters is not None:
							if n.precision_parameters is None:
								n.precision_parameters = {}
							for k,v in i._from.precision_parameters.items():
								if k[:2] != "a_":
									continue
								n.precision_parameters[k[2:]] = v

		for c in constants:
			self.cut_out_node(c)


	def propagate_precisions(self, output_names, target_bits=16):
		to_explore = []
		for n in self.nodes:
			if n.tf_op.name in output_names:
				to_explore.append(n)

		target = target_bits - 1
		def propagate_to_node(current_precision, n):
			nonlocal target
			nonlocal target_bits
			if n.precision_parameters is None:
				n.precision_parameters = current_precision
			elif "a_sign_bits" in n.precision_parameters:
				current_precision = n.precision_parameters.copy()
			else:
				to_add = {}
				for k,v in n.precision_parameters.items():
					to_add["a_" + k] = v
				for k,v in to_add.items():
					n.precision_parameters[k] = v
				current_precision = n.precision_parameters.copy()


			if n.precision_parameters is not None:
				current_precision["a_sign_bits"] = 1
				width = np.sum(current_precision["a_" + k] for k in ["sign_bits", "int", "f"])
				current_precision["a_width"] = width
				if width < target_bits:
					current_precision["a_width"] = target_bits
					current_precision["a_int"] = target_bits - (current_precision["a_f"] + current_precision["a_sign_bits"])

			for i in n.inputs:
				p = None
				if current_precision is not None:
					p = current_precision.copy()
				propagate_to_node(p, i._from)

		for n in to_explore:
			current_precision = n.precision_parameters
			
			
		print("merge_constants", n.precision_parameters)

	def assign_precisions_from_file(self, precisions):
		for n in self.nodes:
			p = precisions[n.tf_op.name]
			if n.precision_parameters is None:
				n.precision_parameters = p
			else:
				for k,v in p.items():
					if v == -1:
						if k not in n.precision_parameters:
							n.precision_parameters[k] = v
						continue
					n.precision_parameters[k] = v
					
	def set_missing_precisions(self, target_act_bits=16, target_parameter_bits=8, target_bias_and_act_func_bits=16):
		def get_precision_names(prefix):
			return {k:prefix + k for k in ["int", "f", "sign_bits"]}

		def guess_missing_precisions(p, example, prefix="a_", pad_int=0, target_bits=16):
			precision_names = get_precision_names(prefix)
			where_nan = np.nonzero(np.isnan(example))
			if where_nan[0].size > 0:
				print(example.shape)
				print(example[where_nan])

			max_magnitude = np.max(np.abs(example))
			if max_magnitude < 1:
				max_magnitude = 1
			p[precision_names["sign_bits"]] = 1
			p[precision_names["int"]] = min(target_bits-2, clog2(max_magnitude)+pad_int)
			p[precision_names["f"]] = target_bits - p[precision_names["int"]] - p[precision_names["sign_bits"]]
			p["min"] = np.min(example)
			p["max"] = np.max(example)

		average_errors = {
			"a_e" : [0.0, 0],
			"e" : [0.0, 0]}
		def _quantize(e, p, prefix):
			nonlocal average_errors
			precision_names = get_precision_names(prefix)
			
			#update min and max and width
			if "min" not in p:
				p["min"] = np.min(e)
			else:
				temp_min = np.min(e)
				if int(temp_min) != int(p["min"]) and temp_min < p["min"]:
					print("WARNING: min specified in quantization parameters", p["min"], "does not match actual min", temp_min)
					p["min"] = temp_min
					
			if "max" not in p:
				p["max"] = np.max(e)
			else:
				temp_max = np.max(e)
				if int(temp_max) != int(p["max"]) and temp_max > p["max"]:
					print("WARNING: max specified in quantization parameters", p["max"], "does not match actual max", temp_max)
					p["max"] = temp_max
			
			e_original = np.array(e, copy=True)  
			q = get_quantized(
				e,
				sign_bits=p[precision_names["sign_bits"]],
				integer_bits=p[precision_names["int"]],
				fractional_bits=p[precision_names["f"]])
			
			e_flat = e_original.flatten()
						
			quantization_error = np.sum(np.abs(q / (2 ** p[precision_names["f"]]) - e_original) / (np.abs(e_original) + 0.00000000001))

			#print("error for this batch:", str(quantization_error/q.size*100) + "%")
			#Marius: If there are only a few values outside the fixed-point range, it might be worth it to add an extra fractional bit and let those values saturate the int bits
			width = p[precision_names["sign_bits"]] + p[precision_names["int"]] + p[precision_names["f"]]
			p["width"] = width
			if width <= 10:			
				#Try quantizing again and re-calculating error
				cur_quant_attempt = 0
				while cur_quant_attempt < QUANT_SATURATION_FACTOR and p[precision_names["int"]] > 0:
					next_lowest_int = 2**(p[precision_names["int"]]-1)
					e_high = e_original[ np.where( e_original > next_lowest_int ) ].flatten()
					e_low = e_original[ np.where( e_original < (-next_lowest_int) ) ].flatten()
					percent_sat_values = (len(e_high)+len(e_low))/len(e_flat)*100
					if percent_sat_values > QUANT_SATURATION_THRESHOLD:
						break
					
					#print("Total values that would saturate:", str(len(e_high)+len(e_low)) + "/" + str(len(e_flat)), str(percent_sat_values) + "%")			
					
					e_copy = np.array(e_original, copy=True) 
					q_2 = get_quantized(
						e_copy,
						sign_bits=p[precision_names["sign_bits"]],
						integer_bits=(p[precision_names["int"]]-1),
						fractional_bits=(p[precision_names["f"]]+1))
					new_quantization_error = np.sum(np.abs(q_2 / (2 ** (p[precision_names["f"]]+1)) - e_original) / (np.abs(e_original) + 0.00000000001))				

					#Add some buffer in case two errors are close (i.e we want a siginificant difference before we start clipping values, as that could end up affecting things in the network)
					if (new_quantization_error/q_2.size*100) < ((quantization_error/q.size*100)-1):
						quant_type = "weight"
						if "a_" in precision_names["int"]:
							quant_type = "activation"							
						print("	Better " + quant_type + " quantization found after moving 1-bit to from int to frac (some values will saturate). New error:", "{:.2f}".format(new_quantization_error/q_2.size*100) + "%", " (Old error:", "{:.2f}".format(quantization_error/q.size*100) + "%)")
						quantization_error = new_quantization_error
						q = q_2
						p[precision_names["int"]] -= 1
						p[precision_names["f"]] += 1
					else:
						break
					
					cur_quant_attempt += 1
								
			average_errors[prefix + "e"][0] += quantization_error
			average_errors[prefix + "e"][1] += q.size		
			
			#print("	Final Error", "{:.2f}".format(quantization_error/q.size*100), "min", p["min"], "max", p["max"])
			#print("	int",p[precision_names["int"]], "frac",p[precision_names["f"]])
			return q

		# Marius: for lower bit precisions adding in activation int padding introduces a huge error\
		# This was originally added in because we can't tell if the activations will overflow off of one sample image
		# With support for multiple sample images this should no longer be the case, but it shouldn't hurt if we have the bits to do it
		activation_padding = 1
		if target_act_bits <= 10:
			activation_padding = 0
			
		for n in self.nodes:
			p = n.precision_parameters
			print(n.tf_op.name, " ", n.type)
			
			cur_param_bits = target_parameter_bits
			cur_act_bits = target_act_bits
			
			#For lower bit precisions override the bias and activation functions with the speicifed value
			#This is in case we only need lower precisions for the multiplication
			if target_act_bits < 16:
				#To find the correct nodes for parameters (i.e. biases) we just need to look for the type
				if n.type not in ("Conv2D", "DepthwiseConv2dNative"):
					cur_param_bits = target_bias_and_act_func_bits
					
				#To find the correct nodes for activations we need to check the all the output to see if it feeds into a convolution
				#If it does feed into a convolution we need to use the convolution activation bits since the output will be converted to this format
				cur_act_bits = target_bias_and_act_func_bits
				for cur_output in n.outputs:
					if cur_output._to.type in ("Conv2D", "DepthwiseConv2dNative"):
						cur_act_bits = target_act_bits
						break
		
			for i,v in enumerate(n.values):
				if "int" not in p or p["int"] is None or p["int"] == -1:
					guess_missing_precisions(p, v, prefix="", target_bits=cur_param_bits)
				n.values[i] = _quantize(v, p, "")
			if "a_int" not in p or p["a_int"] is None or p["a_int"] == -1:
				guess_missing_precisions(p, n.example_outputs[0], prefix="a_", pad_int=activation_padding, target_bits=cur_act_bits)
			_quantize(n.example_outputs[0], p, "a_")

		#print(average_errors)
		average_errors["a_e"] = average_errors["a_e"][0] / average_errors["a_e"][1]
		average_errors["e"] = average_errors["e"][0] / average_errors["e"][1]
		print("        Average parameter quantization error: " + "%.1f" % (average_errors["e"] * 100) + "%")
		print("        Average activation quantization error: " + "%.1f" % (average_errors["a_e"] * 100) + "%")	
		#exit(1)

	def quantize(self):
		def get_precision_names(prefix):
			return {k:prefix + k for k in ["int", "f", "sign_bits"]}

		def _quantize(e, p, prefix):
			precision_names = get_precision_names(prefix)
			q = get_quantized(
				e,
				sign_bits=p[precision_names["sign_bits"]],
				integer_bits=p[precision_names["int"]],
				fractional_bits=p[precision_names["f"]])
			
			return q

		for n in self.nodes:
			p = n.precision_parameters
			n.example_outputs[0] = _quantize(n.example_outputs[0], p, "a_")

    ##########################################################################
	def assign_activation_precisions(self):
		for n in self.nodes:
			if n.tf_op.type != "Sigmoid" and n.tf_op.type != "Tanh" and n.tf_op.type != "Swish" and n.tf_op.type != "swish_f32":
				continue
				
			##output precision
			if n.tf_op.type == "Sigmoid":
				n.precision_parameters = {
					"a_sign_bits" : 1,
					"a_int" : 1,
					"a_f" : 14,
					"a_width" : 16,
					"a_exponent" : 0
				}
			elif n.tf_op.type == "Tanh":
				n.precision_parameters = {
					"a_sign_bits" : 1,
					"a_int" : 1,
					"a_f" : 14,
					"a_width" : 16,
					"a_exponent" : 0
				}
			else: #Swish activation function
				n.precision_parameters = {
				"a_sign_bits" : 1,
				"a_int" : 4,
				"a_f" : 3,
				"a_width" : 8,
				"a_exponent" : 0
			}


			#Input precision
			for e in n.inputs:
				f = e._from
				if f.precision_parameters is None:
					f.precision_parameters = {}
				if n.tf_op.type == "Sigmoid":
					f.precision_parameters["a_sign_bits"] = 1
					f.precision_parameters["a_int"] = 3
					f.precision_parameters["a_f"] = 6
					f.precision_parameters["a_width"] = 10
					f.precision_parameters["a_exponent"] = 0
				elif n.tf_op.type == "Tanh":
					f.precision_parameters["a_sign_bits"] = 1
					f.precision_parameters["a_int"] = 3
					f.precision_parameters["a_f"] = 6
					f.precision_parameters["a_width"] = 10
					f.precision_parameters["a_exponent"] = 0
				else: #Swish activation function
					f.precision_parameters["a_sign_bits"] = 1
					f.precision_parameters["a_int"] = 4
					f.precision_parameters["a_f"] = 5
					f.precision_parameters["a_width"] = 10
					f.precision_parameters["a_exponent"] = 0
					

	####################################################################################
	def add_placeholder_op_for_build_from(self, build_from):
		for n in self.nodes:
			if build_from in n.tf_op.name:
				print(n.tf_op.outputs[0].shape)
				print([o.tf_op.name for o in n.output_nodes()])
				print(n.inputs[0]._from.tf_op.name)
				example_input = n.inputs[0]._from.example_outputs[0]
				precision_parameters = n.inputs[0]._from.precision_parameters
				tf_op = tf.placeholder(dtype=tf.float32, shape=example_input.shape).op
				node = Node(tf_op, [example_input], precision_parameters)
				edge = Edge(node, n)
				node.outputs = [edge]
				n.inputs[0] = edge
				self.heads = [node]
				self.nodes.append(node)
				self.edges.append(edge)
				break

	def split_fused_batch_norms(self):
		fake_tf_graph_node = namedtuple("fake_tf_graph_node", "type name")
		indices_to_remove = []
		for i,n in enumerate(self.nodes):
			if n.tf_op.type not in [
				"FusedBatchNormV3", "FusedBatchNormV2", "FusedBatchNorm"]:
				continue

			gamma_idx = 0
			beta_idx = 1
			mean_idx = 2
			var_idx = 3

			# Batch Norm Learned Params and Statistics
			variance_epsilon = n.tf_op.get_attr("epsilon")
			scale = n.values[gamma_idx] # gamma
			offset = n.values[beta_idx] # beta
			mean = n.values[mean_idx]
			variance = np.abs(n.values[var_idx])

			# New Mul and Bias node values
			scale_values = scale / np.sqrt(variance + variance_epsilon)
			offset_values = offset - (mean * scale_values)

			#print(n.tf_op.name)
			#print(scale_values)
			#print(offset_values)


			mul_op = fake_tf_graph_node("Mul", n.tf_op.name + "_mul")
			bias_add_op = fake_tf_graph_node("BiasAdd", n.tf_op.name + "_add")
			mul_node = Node(mul_op, [n.example_outputs[0] - offset_values])
			mul_node.values.append(scale_values)
			bias_add_node = Node(bias_add_op, n.example_outputs)
			bias_add_node.values.append(offset_values)
			self.swap_inputs(n, mul_node)
			self.swap_outputs(n, bias_add_node)
			indices_to_remove.append(i)
			edge = Edge(mul_node, bias_add_node)
			bias_add_node.inputs.append(edge)
			mul_node.outputs.append(edge)
			self.nodes.extend([mul_node, bias_add_node])
			#for e in bias_add_node.inputs[1:]:
			#	del edges[self.edges.find(e)]
			#del bias_add_node.inputs[1:]

		for i in reversed(indices_to_remove):
			del self.nodes[i]

	def merge_constant_muls(self):
		nodes_to_merge = []
		previous_node = None
		conv_types = ["Conv2D", "DepthwiseConv2dNative"]
		const_mul_nodes = [n for n in self.nodes if n.type == "Mul" and len(n.values) > 0 and len(n.inputs) == 1 and n.inputs[0]._from is not None]
		for n in const_mul_nodes:
			#print([i._from for i in n.inputs])
			all_inputs_conv = np.all([i._from.type in conv_types for i in n.inputs])
			all_outputs_conv = np.all([o._to.type in conv_types for o in n.outputs])
			if not (all_inputs_conv or all_outputs_conv):
				print("Cannot merge mul node: ", n.tf_op.name)
				print(all_inputs_conv)
				print(all_outputs_conv)
				print([i._from.type for i in n.inputs])
				print([i._to.type for i in n.outputs])
				continue

			mul_is_second = all_inputs_conv
			if mul_is_second:
				conv_nodes = [i._from for i in n.inputs]
			else:
				conv_nodes = [o._to for o in n.outputs]

			m = n
			for c in conv_nodes:
				if c.type == "DepthwiseConv2dNative" or (not mul_is_second):
					m_values_with_dims = m.values[0][np.newaxis, np.newaxis, ..., np.newaxis]
				elif c.type == "Conv2D":
					m_values_with_dims = m.values[0][np.newaxis, np.newaxis, np.newaxis, ...]

				"""print(mul_is_second)
				print(c.values[0].shape)
				print(m_values_with_dims.shape)
				print(c.tf_op.name)
				print(m.tf_op.name)
				print(m.inputs[0]._from.tf_op.name)
				print([o._to.tf_op.name for o in c.outputs])
				print([o._from.tf_op.name for o in m.inputs])
				print(m.example_outputs[0].shape)
				print(c.example_outputs[0].shape)"""
				c.values[0] *= m_values_with_dims
				if mul_is_second:
					c.example_outputs[0] = m.example_outputs[0]

			if not mul_is_second:
				self.swap_inputs(m, c)
			else:
				to_remove = []
				for i,e in enumerate(c.outputs):
					if e._to == m:
						to_remove.append(i)
				for i in reversed(to_remove):
					del c.outputs[i]
				for e in m.outputs:
					new_edge = Edge(c, e._to)
					c.outputs.append(new_edge)
					for i,e2 in enumerate(e._to.inputs):
						if e2._from == m:
							e._to.inputs[i] = new_edge
					e._to = None
					e._from = None
				for e in m.inputs:
					e._to = None
					e._from = None
				for e in m.outputs:
					e._to = None
					e._from = None
				assert([e._from is not None and e._to is not None for e in c.outputs])
				assert([e._from is not None and e._to is not None for e in c.inputs])

	def print_mul_node_info(self):
		muls = [n for n in self.nodes if n.type == "Mul"]
		for m in muls:
			print("----------------------")
			print(m.tf_op.name)
			print([(e._from.type, e._from.tf_op.name) for e in m.inputs])
			print([(e._to.type, e._to.tf_op.name) for e in m.outputs])

			#self.cut_out_node(m)

	def swap_bias_adds_and_muls(self):
		def pair_function(a, b):
			if a.type == "BiasAdd":
				#   f_b = a(x+b)
				#       = ax+ab
				# => ax = f_b-ab
				# => new bias = ab

				# The bias add will now be the output of the
				# molecule, and the output of the molecule should
				# remain the same
				np.copyto(a.example_outputs[0], b.example_outputs[0])
				# new bias
				a.values[0] *= b.values[0]
				# ax
				b.example_outputs[0] -= a.values[0]
				#print(np.sum(np.abs(b.example_outputs[0] + a.values[0] - a.example_outputs[0])/np.sum(np.abs(a.example_outputs[0]))))
			else:
				#      f_b = ax+b
				#          = a(x+b/a)
				# => x+b/a = f_b/a
				# => new bias = b/a

				# The mul will now be the output of the
				# molecule, and the output of the molecule should
				# remain the same
				np.copyto(a.example_outputs[0], b.example_outputs[0])
				# x+b/a
				b.example_outputs[0] /= a.values[0]
				# new bias
				b.values[0] /= a.values[0]
				#print(np.sum(np.abs(b.example_outputs[0] * a.values[0] - a.example_outputs[0]))/np.sum(np.abs(a.example_outputs[0])))
		self.swap_nodes_with_types("BiasAdd", "Mul", pair_function)

	def swap_nodes_with_types(self, type_1, type_2, pair_function=None):
		nodes_to_merge = []
		previous_node = None
		def p_node(n):
			nonlocal previous_node
			nonlocal nodes_to_merge
			if previous_node and (
				previous_node.type == type_1 and n.type == type_2 or
				previous_node.type == type_2 and n.type == type_1):
				nodes_tuple = (previous_node, n)
				nodes_to_merge.append(nodes_tuple)
				previous_node = None
			else:
				previous_node = n
		self.walk_graph(p_node)
		for (a,b) in nodes_to_merge:
			original_a_inputs = [i._from for i in a.inputs]
			original_b_inputs = [i._from for i in b.inputs]
			original_a_outputs = [i._to for i in a.outputs]
			original_b_outputs = [i._to for i in b.outputs]
			if pair_function is not None:
				pair_function(a, b)
			self.swap_inputs(a, b)
			self.swap_outputs(b, a)
			assert(np.all([e._from in [n, b] for e, n in zip(a.inputs, original_b_inputs)]))
			assert(np.all([e._from == n for e, n in zip(b.inputs, original_a_inputs)]))
			assert(np.all([e._to == n for e, n in zip(a.outputs, original_b_outputs)]))
			assert(np.all([e._to in [n, a] for e, n in zip(b.outputs, original_a_outputs)]))


	def swap_inputs(self, from_node, to_node):
		tmp = to_node.inputs
		to_node.inputs = from_node.inputs
		from_node.inputs = tmp
		for e in to_node.inputs:
			e._to = to_node
		for e in from_node.inputs:
			e._to = from_node

	def swap_outputs(self, from_node, to_node):
		tmp = to_node.outputs
		to_node.outputs = from_node.outputs
		from_node.outputs = tmp
		for e in to_node.outputs:
			e._from = to_node
		for e in from_node.outputs:
			e._from = from_node

	def set_weights_to_random_with_target_sparsity(self, target_sparsity):
		for n in self.nodes:
			if "Conv" in n.tf_op.name:
				weight_shape = n.values[0].shape
				mean = np.mean(n.values[0])
				std = np.std(n.values[0])
				mask = np.random.binomial(1, 1.-target_sparsity, weight_shape)
				pre_mask_values = np.random.normal(mean, std, weight_shape)
				n.values[0] = (mask * pre_mask_values).astype(n.values[0].dtype)

	def align_add_input_precisions(self):
		for n in self.nodes:
			if n.type in ["Add","AddV2"]:
				max_input_frac_bits = max([e._from.precision_parameters["a_f"] for e in n.inputs])
				num_at_max = np.sum([1 for e in n.inputs if e._from.precision_parameters["a_f"] == max_input_frac_bits])
				if num_at_max > 1:
					frac_bits = max_input_frac_bits
				else:
					frac_bits = max([e._from.precision_parameters["a_f"] for e in n.inputs if e._from.precision_parameters["a_f"] != max_input_frac_bits])

				for _input in n.inputs:
					node = _input._from
					node.precision_parameters["a_f"] = frac_bits

		#exit(1)

class GraphBuilder():
	def get_precisions_from_file(self, path, nodes):
		convert_precision = True
		if not os.path.isfile(path):
			print("    Generating default op precision annotations file")
			with open(path, "w") as fh:
				fh.write("node_name,scale,sign_bits,exponent,int,f,a_sign_bits,a_exponent,a_int,a_f\n")
				for node in nodes:
					fh.write(",".join([node.tf_op.name,"1.","1","0","-1","-1","1","0","-1","-1"]) + "\n")
		precisions = {}
		with open(path, "r") as fh:
			ann = fh.read().split("\n")
			keys = ann[0].split(",")
			for l in ann[1:]:
				if l == "":
					continue
				arr = l.split(",")
				precisions[arr[0]] = {}
				for i,v in enumerate(arr[1:]):
					v = float(v)
					if v.is_integer():
						v = int(v)
					precisions[arr[0]][keys[i+1]] = v
		return precisions

	def print_graph_nodes(self, nodes):
		for n in nodes:
			print(n.type,": ", n.name, end="")
			if len(n.outputs) > 0:
				print(": ", n.outputs[0].name, end="")
			print()


	def load_graph(self,
		path,
		example_image,
		output_names,
		op_precision_annotations_path=None,
		target_act_bits=16,
		target_parameter_bits=8,
		target_bias_and_act_func_bits=16,  
		generate_example_outputs=True,
		import_name="import",
		build_from=None,
		make_fake_weights=False,
		fake_weights_sparsity=0.0,
		print_graph_nodes=False,
		import_mobilenet_ssd=False, **kwargs):
		#Allow memory growth on GPUs otherwise we run out
		config = tf.ConfigProto()
		config.gpu_options.allow_growth = True
		with tf.Session(config=config) as sess:
			with open(path, "rb") as f:
				gd = tf.GraphDef()
				gd.ParseFromString(f.read())
			nodes = get_nodes(gd, import_name=import_name)
			if print_graph_nodes:
				self.print_graph_nodes(nodes)
				exit(0)

			example_feed_dict = None
			multi_example_feed_dict = None
			multiple_images = False
			for node in nodes:
				if node.type == "Placeholder":
					
					placeholder = node
					if not generate_example_outputs:
						continue
					if example_image is None:
						shape = []
						for d in placeholder.outputs[0].shape:
							if d.value is None:
								shape.append(1)
							else:
								shape.append(d.value)
						tmp = np.random.rand(*shape)
					else:
						tmp = example_image
						if example_image.shape[0] > 1:
							multiple_images = True
							split_ex_image = np.split(example_image, example_image.shape[0])
						
					if example_feed_dict is None:
						example_feed_dict={placeholder.outputs[0].name: tmp}
					else:
						example_feed_dict[placeholder.outputs[0].name] = tmp
						
					#Create a list of example feed dicts in case we need to feed them one at a time
					if multiple_images:
						if multi_example_feed_dict == None:
							multi_example_feed_dict = []
							for ex_image in split_ex_image:
								multi_example_feed_dict.append({placeholder.outputs[0].name: ex_image})
						else:
							for i in range(0,len(split_ex_image)):
								multi_example_feed_dict[i][placeholder.outputs[0].name] = split_ex_image[i]
			
			producer_for_tensor = get_tensor_to_producer_op_map(nodes)
			consumers_for_tensor = get_tensor_to_consumer_op_map(nodes)
			heads = []
			internal_heads = []
			gnodes = []
			example_outputs = [None for n in nodes]
			for node, output_examples in zip(nodes, example_outputs):
				gn = Node(node, output_examples)
				if gn.type == "Placeholder":
					heads.append(gn)
				gnodes.append(gn)

			print("    Building HPIPE Operation Graph")
			edges = []
			for gn in gnodes:
				for output in gn.tf_op.outputs:
					if output.name not in consumers_for_tensor:
						continue
					consumers = consumers_for_tensor[output.name]
					for consumer in consumers:
						for gn2 in gnodes:
							if gn2.tf_op == consumer:
								e = Edge(gn, gn2)
								edges.append(e)
								gn2.add_input(e)
								gn.add_output(e)
								break
			# reorder node inputs to match tensorflow
			for gn in gnodes:
				new_edge_list = []
				for _input in gn.tf_op.inputs:
					for e in gn.inputs:
						if _input in e._from.tf_op.outputs:
							new_edge_list.append(e)
							break
				assert(len(new_edge_list) == len(gn.inputs))
				gn.inputs = new_edge_list

			graph = Graph()
			graph.internal_heads = internal_heads
			graph.heads = heads
			graph.nodes = gnodes
			graph.edges = edges


			if output_names is None or len(output_names) < 1:
				print("No output_names specified.  Please add some to the output_names field in the params file.")
				print("Possible names for this network are:")
				for n in graph.nodes:
					print("    " + n.tf_op.name)
				sys.exit(0)

			"""for node in graph.nodes:
				if "BoxPredictor" in node.tf_op.name:
					value = None
					if node.type == "Const":
						value = sess.run(node.tf_op.outputs[0])
					print(node.type, " ", node.tf_op.name, " ", [i.tf_op.name for i in node.input_nodes()], " ", value)
			sys.exit(0)"""

			print_node_count = lambda: print(len(graph.nodes))
			print_node_count()
			graph.prune_from_output(output_names)
			print_node_count()

			graph_nodes = graph.nodes

			if import_mobilenet_ssd:
				graph_nodes = [node for node in graph_nodes if "Preprocess" not in node.tf_op.name]
				for n in graph.nodes:
					if n.type == "Placeholder":
						p = n
				to_explore = [p]

				while len(to_explore) > 0:
					n = to_explore.pop(0)
					to_explore.extend(n.output_nodes())
					if "import/FeatureExtractor/MobilenetV1/MobilenetV1/Conv2d_0/BatchNorm/batchnorm/add_1" in n.tf_op.name:
						break
					try:
						output_tensor = sess.run(n.tf_op.outputs[0], feed_dict=example_feed_dict)
						print(n.tf_op.name, " ", n.type, " ", output_tensor.shape)
						n.example_outputs = [output_tensor]
					except:
						continue

			if generate_example_outputs:
				"""to_explore = [node for node in graph.nodes if node.tf_op.name in output_names]
				tf_outputs = []
				while len(to_explore) > 1:
					n = to_explore.pop(0)
					tf_outputs.append.append(to_skip)
					to_explore.extend(to_skip.input_nodes())

				nodes_to_skip = []
				if build_from is not None:
					head = None
					for node in graph.nodes:
						if build_from in node.tf_op.name:
							head = node
							break
					to_explore = [] + head.input_nodes()"""

				nodes_used = [node for node in graph_nodes if len(node.tf_op.outputs) > 0]
				tf_outputs = [[output for output in node.tf_op.outputs] for node in nodes_used]
				print("    Computing example outputs for each layer")
				global_vars          = tf.global_variables()
				is_not_initialized   = sess.run([tf.is_variable_initialized(var) for var in global_vars])
				not_initialized_vars = [v for (v, f) in zip(global_vars, is_not_initialized) if not f]
				if len(not_initialized_vars):
					sess.run(tf.variables_initializer(not_initialized_vars))
				tf_outputs = [o[0] for o in tf_outputs if len(o) > 0]
				#for o in tf_outputs:
					#print(o)
					#print(o[0].name)
					#sess.run(o[0], feed_dict=example_feed_dict)
					
				#If this fails it probably means the input shape does not match the placeholder (i.e. we are using multiple example images)
				#This only happens in resnet and can be fixed by running one example image at a time
				try:
					example_outputs = sess.run(tf_outputs, feed_dict=example_feed_dict)
				except:
					# Raise the exception again since it is not related to multiple images
					if not multiple_images:
						example_outputs = sess.run(tf_outputs, feed_dict=example_feed_dict)
						
					del example_feed_dict
					
					#Run each image individually
					example_outputs_list = []
					for cur_ex_dict in multi_example_feed_dict:
						example_outputs_list.append(sess.run(tf_outputs, feed_dict=cur_ex_dict))
					
					#Concatinate the results together. Ignore constant nodes since those are biases/weights which don't depend on multiple images
					for i in range(len(example_outputs_list[0])):
						if nodes_used[i].type != "Const":
							for j in range(1, len(example_outputs_list)):
								example_outputs_list[0][i] = np.concatenate((example_outputs_list[0][i], example_outputs_list[j][i]))					
					example_outputs = example_outputs_list[0]
					del example_outputs_list
				
				
				example_outputs = [[o] for o in example_outputs]
				
				for e,n in zip(example_outputs, nodes_used):
					n.example_outputs = e
					cur_var = n.example_outputs
					#print(n.type, n.example_outputs[0].shape)

				for n in graph.nodes:
					if "BoxPredictor" in n.tf_op.name:
						print(n.tf_op.name)

			if build_from is not None:
				print_node_count()
				print_node_count()
				print("    Adding placeholder op for build from")
				graph.add_placeholder_op_for_build_from(build_from)
				print_node_count()
				graph.prune_from_output(output_names)
			print_node_count()
			
			print("    Merging any quant ops (if they exist)")
			graph.merge_quant_ops()
			print_node_count()
			#############################################
			print("    Setting fixed Sigmoid, Tanh and Swish precisions")
			graph.assign_activation_precisions()
			#############################################
			print("    Propagating Precisions from Outputs")
			#graph.propagate_precisions(output_names, target_bits=target_act_bits)
			print_node_count()
			print("    Merging all constants into their children")
			graph.merge_constants()
			print_node_count()
			print("    Removing Identity nodes")
			graph.cut_out_nodes_of_type("Identity")
			graph.cut_out_nodes_of_type("IdentityN")
			print_node_count()
			print("    Folding Batch Norms")
			print("    Splitting Batch Norms")
			graph.split_fused_batch_norms()
			print_node_count()
			print("    Merging Constant Muls")
			graph.merge_constant_muls()
			print_node_count()
			print("    Swapping Bias Add and Mul")
			graph.swap_bias_adds_and_muls()
			print_node_count()
			print("    Merging Constant Muls")
			graph.merge_constant_muls()
			print_node_count()
			print("    Swapping Bias Add and Mul")
			graph.swap_bias_adds_and_muls()
			graph.prune_dead_nodes()
			print_node_count()
			print("    Pruning nodes not affecting outputs: " + str(output_names))
			graph.prune_from_output(output_names)
			print_node_count()
			print("    Pruning nodes disjoint from the primary graph")
			graph.prune_dead_nodes()
			graph.prune_from_output(output_names)
			print_node_count()
			print("    Converting fully connected layers to convolutions and constant Adds into BiasAdds")
			graph.convert_dense_to_conv2d()
			graph.convert_add_constant_to_bias_add()
			print_node_count()
			#graph.prune_zero_ocs()
			print("    Merging explicit padding operations into convolutions")
			graph.push_pad_ops_into_convs()
			print_node_count()
			print("    Removing Reshape nodes")
			graph.cut_out_nodes_of_type("Reshape")
			graph.cut_out_nodes_of_type("Squeeze")
			print_node_count()
			print("    Replacing average pooling layers with mean layers")
			graph.replace_avg_pool_with_mean()
			print_node_count()
			graph.prune_dead_nodes()
			graph.prune_from_output(output_names)
			for n in graph.nodes:
				if "BoxPredictor" in n.tf_op.name:
					print(n.tf_op.name)
			print_node_count()
			print("    Overriding default precisions with annotations from file")
			graph.assign_precisions_from_file(self.get_precisions_from_file(op_precision_annotations_path, graph.nodes))
			print("    Quantizing Examples and Parameters")
			graph.set_missing_precisions(target_act_bits=target_act_bits, target_parameter_bits=target_parameter_bits, target_bias_and_act_func_bits=target_bias_and_act_func_bits)
			graph.align_add_input_precisions()
			graph.quantize()
			if make_fake_weights:
				graph.set_weights_to_random_with_target_sparsity(fake_weights_sparsity)
			for n in graph.nodes:
				print(n.tf_op.name, " ", n.type)


		return graph
"""
	def load_graph(self, path, example_image, output_names, op_precision_annotations_path=None, target_bit_width=8, generate_example_outputs=True, import_name="import", optimize_graph=True):
		print("  Building Graph")
		convert_precision = False
		example_feed_dict = None
		with tf.Session() as sess:
			with tf.gfile.GFile(path, "rb") as f:
				gd = tf.GraphDef()
				gd.ParseFromString(f.read())
			nodes = get_nodes(gd, import_name=import_name)
			placeholder = None
			for node in nodes:
				if node.type == "Placeholder":
					placeholder = node
					if not generate_example_outputs:
						continue
					if example_image is None:
						tmp = np.random.rand(*placeholder.outputs[0].shape)
					else:
						tmp = example_image
					if example_feed_dict is None:
						example_feed_dict={placeholder.outputs[0].name: tmp}
					else:
						example_feed_dict[placeholder.outputs[0].name] = tmp

			producer_for_tensor = get_tensor_to_producer_op_map(nodes)
			consumers_for_tensor = get_tensor_to_consumer_op_map(nodes)
			heads = []
			internal_heads = []
			gnodes = []

			if op_precision_annotations_path is not None:
				convert_precision = True
				if not os.path.isfile(op_precision_annotations_path):
					print("    Generating default op precision annotations file")
					with open(op_precision_annotations_path, "w") as fh:
						fh.write("node_name,scale,sign_bits,exponent,int,f,a_sign_bits,a_exponent,a_int,a_f\n")
						for node in nodes:
							if not isinstance(node, tf.Operation):
								continue
							fh.write(",".join([node.name,"1.","1","0","-1","-1","1","0","-1","-1"]) + "\n")
				precisions = self.get_precisions_from_file(op_precision_annotations_path)


			tf_outputs = [[output for output in node.outputs] for node in nodes if isinstance(node, tf.Operation)]
			if generate_example_outputs:
				print("    Computing example outputs for each layer")
				example_outputs = sess.run(tf_outputs, feed_dict=example_feed_dict)
				print("    Casting example outputs to fixed point representation")
			else:
				example_outputs = [None for n in nodes]
			for node, output_examples in zip(nodes, example_outputs):
				if not isinstance(node, tf.Operation):
					continue
				if node.type == "Const":
					continue
				precision_parameters = None
				if output_examples is not None and node.name in precisions.keys():
					precision_parameters = precisions[node.name]
					for i,e in enumerate(output_examples):
						if type(e) is list:
							assert len(e) == 1
							e = e[0]
						if np.issubdtype(e.dtype, np.integer):
							continue
						target_bits = target_bit_width
						target_bits -= precision_parameters["a_sign_bits"]
						if precision_parameters["a_int"] == -1:
							max_magnitude = np.max(np.abs(e))
							if max_magnitude <= 0.0:
								continue
							precision_parameters["a_int"] = min(target_bits, clog2(max_magnitude))
							precision_parameters["a_f"] = target_bits - precision_parameters["a_int"]
						output_examples[i] = get_quantized(
							e,
							sign_bits=precision_parameters["a_sign_bits"],
							integer_bits=precision_parameters["a_int"],
							fractional_bits=precision_parameters["a_f"])
				gn = Node(sess, node, output_examples, precision_parameters=precision_parameters)
				if gn.type == "Placeholder":
					heads.append(gn)
				for _input in node.inputs:
					try:
						gn.values.append(sess.run(_input))
					except:
						continue
					# We have just proven we can statically determine the value this node produces,
					# so to simplify support we will label it a constant and remove its inputs
					# to prune any potentially unsupported ops that help to produce this value
					if producer_for_tensor[_input.name].type != "Const":
						new_name = _input.name
						try:
							index = _input.name.index(":")
							new_name = new_name[:index]
						except:
							pass
						new_constant_tensor = tf.constant(gn.values[-1], dtype=tf.float32, name=new_name)
						constant_tensor_op = new_constant_tensor.op
						#consumers_for_tensor[constant_tensor_op.outputs[0].name] = consumers_for_tensor[producer_for_tensor[_input.name].outputs[0].name]
						if producer_for_tensor[_input.name].outputs[0].name in consumers_for_tensor:
							del consumers_for_tensor[producer_for_tensor[_input.name].outputs[0].name]
						#producer_for_tensor[_input.name] = constant_tensor_op

					if convert_precision and _input.dtype != tf.int32 and node.name in precisions.keys():
						params = precisions[node.name]
						target_bits = target_bit_width
						target_bits -= precision_parameters["sign_bits"]
						if params["int"] == -1:
							max_magnitude = np.max(np.abs(params["scale"] * gn.values[-1]))
							if max_magnitude <= 0.0:
								continue
							params["int"] = min(target_bits, clog2(max_magnitude))
							params["f"] = target_bits - params["int"]
						gn.values[-1] = get_quantized(
							params["scale"] * gn.values[-1],
							sign_bits=params["sign_bits"],
							integer_bits=params["int"],
							fractional_bits=params["f"])
				gnodes.append(gn)

		print("    Building HPIPE Operation Graph")
		edges = []
		for gn in gnodes:
			for output in gn.tf_op.outputs:
				if output.name not in consumers_for_tensor:
					continue
				consumers = consumers_for_tensor[output.name]
				for consumer in consumers:
					for gn2 in gnodes:
						if gn2.tf_op == consumer:
							e = Edge(gn, gn2)
							edges.append(e)
							gn2.add_input(e)
							gn.add_output(e)
							break
		graph = Graph()
		graph.internal_heads = internal_heads
		graph.heads = heads
		graph.nodes = gnodes
		graph.edges = edges
		if output_names is None or len(output_names) < 1:
			print("No output_names specified.  Please add some to the output_names field in the params file.")
			print("Possible names for this network are:")
			for n in graph.nodes:
				print("    " + n.tf_op.name)
			sys.exit(0)
		if optimize_graph:
			print("    Pruning nodes not affecting outputs: " + str(output_names))
			graph.prune_from_output(output_names)
			print("    Pruning nodes disjoint from the primary graph")
			graph.prune_dead_nodes()
			print("    Converting fully connected layers to convolutions and constant Adds into BiasAdds")
			graph.convert_dense_to_conv2d()
			graph.convert_add_constant_to_bias_add()
			graph.merge_quant_ops()
			#graph.prune_zero_ocs()
			print("    Merging explicit padding operations into convolutions")
			graph.push_pad_ops_into_convs()
			print("    Removing Reshape nodes")
			graph.cut_out_nodes_of_type("Reshape")
			print("    Removing Identity nodes")
			graph.cut_out_nodes_of_type("Identity")
			print("    Replacing average pooling layers with mean layers")
			graph.replace_avg_pool_with_mean()
			graph.prune_dead_nodes()
		return graph
"""






'''
	def load_graph(self, path):
		"""
		The OP dump was designed for SCNN, which makes it slightly strange
		for the purposes of this architecture.  Each "OP" could perform
		multiple convolutions and multiple adds that were reduced by
		an implied Add op.  In addition, each OP could have multiple
		readback operations

		"""
		graph = Graph()
		op_names = get_lines_in_file(path + "/ops.txt")
		for op_name in op_names:
			inputs, nodes, edges, outputs = self._load_op(path + "/" + op_name)

	def _load_op(self, path):
		component_names = get_lines_in_file(path + "/layer_names.txt")
		components = {}
		for cn in component_names:
			components[cn] = self._get_component_data(path + "/" + cn)

	def _get_component_data(self, path):
		component_data = {}
		files = get_lines_in_file(path + "/layer_components.txt")
		component_data["type"] = get_lines_in_file(path + "/type.txt")[0]
		for fn in files:
			component_data[fn] = np.load(path + "/" + fn + ".npy")
		input_index_path = path + "/input_index.txt"
		if os.path.exists(input_index_path):
			component_data["input_index"] = int(get_lines_in_file(input_index_path)[0])
		return component_data
'''
