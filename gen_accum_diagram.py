import numpy as np
np.random.seed(10)
g_scale_weights = (np.random.rand(3,7,7,1) > 0.90).astype(float)
weights = np.random.rand(3,7,7,3) * g_scale_weights

kernel_range = np.arange(7.)
act_mask = np.matmul(kernel_range[..., np.newaxis], kernel_range[np.newaxis, ...])[np.newaxis, ..., np.newaxis]
act_mask = np.logical_and(act_mask > 6, act_mask < 24).astype(float)

act_values = np.concatenate([((kernel_range + 10.)/16.)[..., np.newaxis], (kernel_range/6.)[..., np.newaxis], np.full([7,1], 0.01)], axis=1)[np.newaxis, np.newaxis, ...]
act_values = np.tile(act_values, [1,7,1,1])
acts = act_values * act_mask
acts = np.pad(acts, [[0,0], [6,6], [6,6], [0,0]], mode="constant", constant_values=0.)

output = np.zeros([3, 7+12, 7+12, 3])

for i in range(13):
	for j in range(13):
		output += np.pad(weights * acts[:,i:i+7,j:j+7], [[0,0],[12-i,i],[12-j,j],[0,0]], mode="constant", constant_values=0.)
		print(len(np.nonzero(output)[0]))


def print_np_as_cubes(t):
	shape = t.shape[:-1]
	color_t = r"\definecolor{tmp}{rgb}{%f,%f,%f};"
	cube_t = r"\cube{%d}{%d}{%d}{%d}{%d}{%d}{%s};"
	print(cube_t % (-shape[2], -shape[1], shape[0], shape[2] / 2.0 - 0.5, shape[1] / 2.0 - 0.5, shape[0] / 2.0 - 0.5, ""))
	for z in range(shape[0]):
		for y in range(shape[1]):
			for x in range(shape[2]):
				if len(np.nonzero(t[z,y,x,:])[0]) == 0:
					continue
				print(color_t % tuple([min(t[z,y,x,i],1) for i in range(3)]))
				print(cube_t % tuple([1,1,1, x,y,z, "fill=tmp,draw=tmp!70!black"]))
	print(cube_t % (shape[2], shape[1], shape[0], shape[2] / 2.0 - 0.5, shape[1] / 2.0 - 0.5, shape[0] / 2.0 - 0.5, ""))

#print_np_as_cubes(weights[:,6:,6:,:])
print_np_as_cubes(weights)
print()
print_np_as_cubes(output)
r"""
\begin{tikzpicture}[yscale=.2,xscale=0.2]
\pgfmathsetseed{473};
\setcounter{weightcolour}{0};
\cube{-13}{-13}{3}{26}{2.5}{1}{};
\cube{-7}{-7}{3}{3}{3}{1}{};
\foreach \x in {6,...,0} {
    \foreach \y in {6,...,0} {
        \foreach \z [
            evaluate=\z as \r using {(rand+1)*0.5},
            evaluate=\z as \g using {(rand+1)*0.5},
            evaluate=\z as \b using {(rand+1)*0.5},
            evaluate=\z as \ra using {int(rand*20)}
            ] in {0, ..., 2}{
            \ifnum \ra>17\relax
                \definecolor{weight\the\value{weightcolour}}{rgb}{\r, \g, \b};
                \cube{1}{1}{1}{\x}{\y}{\z}{fill=weight\the\value{weightcolour},draw=weight\the\value{weightcolour}!70!black};
                \foreach \xb in {0,...,6} {
                    \foreach \yb [
                        evaluate=\yb as \rb using {\r*(\xb+10)/16},
                        evaluate=\yb as \gb using {\g*\xb/6},
                        evaluate=\yb as \bb using {\b*0.01},
                        evaluate=\yb as \sd using {int(\xb*\yb)}]
                        in {0,...,6} {
                        \ifnum \sd<24\relax
                        \ifnum \sd>6\relax
                            \definecolor{tmp}{rgb}{\rb,\gb,\bb};
                            \begin{scope}[]
                                \cube{1}{1}{1}{\x+\xb+20}{\y+\yb-3.5}{\z}{fill=tmp,draw=tmp!50!black};
                            \end{scope}
                            %\cube{1}{1}{1}{\x+\xb+20}{\y+\yb-3.5}{\z}{draw=tmp!70!black};
                        \fi
                        \fi
                    }
                }
                \addtocounter{weightcolour}{1};
            \fi
        }
    }
}
\cube{13}{13}{3}{26}{2.5}{1}{};
\cube{7}{7}{3}{3}{3}{1}{};
\draw[->] (3.5,6.7,-0.5) to [out=40,in=160] node[midway, above, anchor=south, align=center] {\footnotesize Multiply all weights with all activations in one input channel,\\[-2pt]\footnotesize scatter, and accumulate into another buffer} (20,8.7,0.5);
\node [align=center,anchor=north](sw) at (3.5,-0.5,3) {\small Sparse Weights\\[-5pt]\tiny for multiple output channels\\[-5pt]\tiny and one input channel};

\begin{scope}[shift={(10,0)}]
\pgfmathsetseed{532};
\setcounter{actcolour}{0};
\cube{-7}{-7}{1}{3}{3}{0}{};
\foreach \x in {0,...,6} {
    \foreach \y in {0,...,6} {
        \foreach \z [
            evaluate=\z as \r using {(\x+10)/16},
            evaluate=\z as \g using {\x/6},
            evaluate=\z as \b using {0.01},
            evaluate=\z as \ra using {int(\x * \y)}
            ] in {0}{
            \ifnum \ra<24\relax
                \ifnum \ra>6\relax
                \definecolor{act\the\value{actcolour}}{rgb}{\r, \g, \b};
                \cube{1}{1}{1}{\x}{\y}{\z}{fill=act\the\value{actcolour},draw=act\the\value{actcolour}!70!black};
                \addtocounter{actcolour}{1};
                \fi
            \fi
        }
    }
            
}
\node[] at (-1.5,3.5,1) {$*$};
\node[] at (8,3.5,1) {$=$};
%\cube{7}{7}{3}{3}{3}{1}{fill=orange, opacity=0.2, draw=none};
\cube{7}{7}{1}{3}{3}{0}{};
\node [align=center,anchor=north] at (3.5,-0.5,1) {\footnotesize One Input Channel\\[-2pt]\footnotesize of Activations};
\end{scope}
\begin{scope}[shift={(550pt,0)}]
\addtocounter{weightcolour}{-1};
%\foreach \y in {0, ..., \the\value{weightcolour}} {
%    \cube{1}{1}{1}{0}{\y}{1}{fill=test};
%}
\end{scope}
\end{tikzpicture}
"""