.. _Code Flow:
Code Flow
========================================

This guide will explain the flow of the Python generation script for new users to allow for easier development. Please take a look at the :doc:`Quick Start Guide <../usage/quickstart>` first for an overview on how to setup/run the flow.

Initialization
----------------------------

The main python script is ``build_accelerator.py``, all other functions are called from this script. First, the arguments are extracted from the specify parameter json and the ``get_plan`` function applies the input adjustments to the specified sample image. This function calls the TensorFlow optimization function and the planner before returning. The main function then calls the ``walk_graph`` function in order to build the circuit and generate the Verilog.

Optimizing The Tensorflow Graph
----------------------------

This stage uses ``load_graph`` inside ``GraphBuilder.py`` in order to optimize the TensorFlow graph before attempting to convert it into a hardware circuit. Example outputs are first computed using the specified sample image. These outputs are used to set the fixed point precisions for each layer if they have not been explicitly specified.

Next, the script runs the following optimizations:

1. Merging any quant ops
	This optimization finds any ``FakeQuant`` TensorFlow ops---which are nodes used to quantize a floating point number to any quantization scheme (you can specify a minimum and maximum value and the number of bits used to represent the number between those two values)---and uses them to set the precision for their input nodes.  We only support symmetric power-of-2 quantization, so our formula for the number of integer bits is :math:`\lceil\log_2|\textrm{max}(min,max)|\rceil`.  Our number of fractional bits is then the ``num_bits`` attribute from the tensorflow op minus the number of bits allocated to the integer portion and the sign bit.

2. Setting fixed swish/sigmoid/tanh precisions
	Assigns the fixed point precisions for the nodes going into the activation function. This also determines the size of the LUT used for the activation function. For example, if ``a_width`` is 10 then the LUT will contain 1024 entries. The output precision paramters are also set in this stage. Note that increasing the precisions too much will result in high M20K usage.
	
3. Merging all constants into their children
	TensorFlow represents constant values as their own nodes in a graph.  For us it is more convenient to have constants stored not as inputs, but as a property of the node.  We find all constants in the graph and then remove them as inputs, but attach the values to the child nodes.  We also copy the precision from the constant node; the ``a_*`` precisions indicate that they are output precisions, the ones without the prefix indicate they apply to parameters on the node.
	
4. Removing Identity nodes
	Identity nodes just pass the input to the output and get in the way of our other optimizations, so we remove them and assign their inputs to their outputs.
	
5. Folding and splitting batch norms
	We first break batch normalization operations up into a BiasAdd and a Mul node.  Both of these are constant operations, so we can factor them out of the computation and merge them with other nodes in the next optimization.

6. Merging Constant Muls and Swapping Bias Add and Mul
	We can merge the Mul ops with adjacent Conv2D or DepthwiseConv2D ops, and we can merge the BiasAdd ops with adjacent BiasAdd ops.  After we do one pass of merging these nodes, we then try to merge more by swapping remaining Mul nodes that are adjacent to BiasAdds (re-factor the values so that :math:`m(a+b)` becomes :math:`ma+mb` or vice-versa), then try to merge these, then swap again.

7. Pruning nodes not affecting outputs or disjoint from the primary graph
	We walk the graph going through the inputs to any of the nodes with names specified in the ``output_names`` list in the .json config file used to configure the accelerator.

8. Converting fully connected layers to convolutions and constant Adds into BiasAdds
	In TensorFlow fully connected layers are matrix multiplication nodes, and in some networks they use BiasAdd ops to add constants, but in others they use Add nodes.  We just rename all of these nodes to be other ops we know how to handle.  In the case of the fully connected layers we also have to transform the weight tensor since a matrix multiplication expects rank-2 tensors while a convolution expects rank-4.

9. Merging explicit padding operations into convolutions
	TensorFlow v1.* used two types of padding, explicit and automatic.  Explicit padding came from padding nodes in the graph that added explicit padding to its inputs.  Automatic padding comes from the ``"SAME"`` and ``"VALID"`` parameters passed to the convolution ops.  We merge the explicit padding onto the convolution or MaxPooling nodes as an additional parameter in this stage.  TensorFlow 2.* now has ``explicit_padding`` as another parameter on convolution nodes and we do not yet support this.

10. Removing Reshape nodes
	This is a hack and we should have better error checking here.  There is one reshape node in each of our ResNet-50 and MobileNet models.  It is effectively a Squeeze node that converts the rank-4 input to the fully-connected layers to a rank-2 tensor.  We converted the fully connected layers to convolutions, so we actually want the rank-4 input.  As a result, for these cases we can simply remove all of the reshape nodes from the graph.  Clearly this will break other graphs, and we should at the very least add error messages that inform a user when we encounter an unsupported Reshape node.

11. Replacing average pooling layers with mean layers
	Many networks use a mean layer to average the features extracted by the convolutional layers before they are run through the fully connected layers.  This reduces the spatial dimensionality to 1x1.  In other networks they do the same thing but with an average pool layer with a kernel shape equal to the input spatial dimension shape.  We re-name these operations to be mean layers, but we likely don't have appropriate error checking to ensure we only do this in supported cases.


Devising a Plan
----------------------------

This stage uses the class ``__init__`` function in ``Plan.py`` to determine the resource and throughput estimations for the graph, which then allows it to form a plan and determine things like DSP usage. Things like the size of the network and the channel split limit (amount of parallelization we want to extract) can greatly affect the runtime, and thus multiprocessing is enabled on the two innermost loops using the ``cycle_estimate_loop`` function.


Building The Circuit
----------------------------

This stage uses the ``main`` function inside of ``LayerComponents.py`` in order to create a digital logic circuit for each function in the TensorFlow graph. The ``module_map`` is used to map the TensorFlow node type to a generation function.

Main Functions:
	BasicConv
		Insert description here
		
	MaxPool
		Insert description here
		
	BiasAdd
		Insert description here

	Sigmoid, Swish, & Tanh
		Each activation function is implemented as a LUT using M20K blocks on the FPGA. The number of entries in each table is determined based off of ``a_width`` when assigning the precisions inside ``GraphBuilder.py``.  A memory delay is also added to the LUT/ROM.
		
	ReLU
		The ReLU is implemented as a mux, where anything below 0 is clipped to 0 and anything above 0 has an output identical to the input.

Important Helper Functions:
	Input Activation Buffer
		Insert description here
	

Generating The Verilog
--------------------------

This stages uses the ``digitallogic`` library to generate the Verilog files.

