import numpy as np
import math
import sys
from matplotlib import pyplot as plt
classes = list(np.fromfile("generated_files/binary_inputs/classes.bin", np.int16))

def load_bin_predicions(fh):
	read_int = lambda: np.frombuffer(fh.read(4), dtype=np.uint32)[0]
	#print(np.unpackbits(np.frombuffer(fh.read(4*4), dtype=np.ubyte)))

	#sys.exit(0)
	dims = {n : read_int() for n in ["c", "h", "w"]}

	pcie_transfer_bits = 256
	pcie_transfer_bytes = pcie_transfer_bits // 8

	bits = read_int()
	print("dims", dims, "bits", bits)
	values_per_transfer = pcie_transfer_bits // bits
	print("values_per_transfer", values_per_transfer)

	output_size = np.prod(list(dims.values()))
	print(output_size)
	fractional_transfers_per_output = output_size / values_per_transfer

	integer_transfers_per_output = int(fractional_transfers_per_output)
	total_transfers_per_output = math.ceil(fractional_transfers_per_output)
	fraction_of_normal_transfer_in_final_transfer = fractional_transfers_per_output - integer_transfers_per_output

	values_in_final_transfer = int(fraction_of_normal_transfer_in_final_transfer * values_per_transfer + 0.5)
	bytes_for_final_transfer = math.ceil(values_in_final_transfer * bits / 8)
	final_transfer_padding = pcie_transfer_bytes - bytes_for_final_transfer

	bytes_per_output = total_transfers_per_output * pcie_transfer_bytes #integer_transfers_per_output * pcie_transfer_bytes + bytes_for_final_transfer

	data = np.frombuffer(fh.read(), dtype=np.ubyte)
	#data = data[:int(output_size*2)]
	#data = data.reshape([-1, total_transfers_per_output, pcie_transfer_bytes])
	#data = np.pad(data, [[0,0], [0, final_transfer_padding]], mode="constant", constant_values=0)
	#data = data.reshape([-1, total_transfers_per_output, pcie_transfer_bytes])

	# Takes each bit in each byte and gives it its own element in the array
	# e.g. [5, 4] becomes [0,0,0,0,0,1,0,1, 0,0,0,0,0,1,0,0] if [5, 4] is of type np.ubyte
	data = np.unpackbits(data)

	# Reshape it so that there are two dimensions, the inner dim contains bits
	# the outer dim contains bytes
	data = np.reshape(data, [-1,8])

	# We want a continuous stream of bits so that we can concatenate bits from
	# different bytes to become the same number.  Unfortunately the data comes
	# in in the opposite order, so if we had a 12 bit number that had a value of
	# 448 we get [[1,1,0,0,0,0,0,0],[0,0,0,0,0,0,0,1]], but if we want these
	# to be contiguous when we concatenate them we need to flip them.  We
	# probably could have flipped the bytes instead of the bits and not
	# needed to flip them back, but I coded it this way, so we get
	# [[0,0,0,0,0,0,1,1], [1,0,0,0,0,0,0,0]].  This makes it so that we will
	# be able to concatenate the bytes together (note that this is a 12 bit 
	# representation of 484 backwards, so from least significant to most significant,
	# we have: [0,0,0,0,0,0,1,1,1,0,0,0]).
	data = np.flip(data, -1)

	# Now reshape it into the PCIe transfers.  We have 256 bits for the pcie
	# transfer as our last dimension, then we have the number of transfers
	# for a single output in the middle dimension, then the -1 is for the
	# total number of outputs in the binary.
	data = data.reshape([-1, total_transfers_per_output, pcie_transfer_bits])
	#print(data[0,0,:])

	# Here we just remove any padding if we 0 padded the pcie_transfer size
	transfer_bits = values_per_transfer * bits
	data = data[:,:,:transfer_bits]

	# Now we can break the values per transfer up not into bytes, but into
	# the bits in the output data elements (e.g. 12 bits)
	data = data.reshape([-1, total_transfers_per_output, values_per_transfer, bits])

	# Just find the smallest numpy data type that is greater than or equal to
	# the number of bits in our elements (e.g. for 12 we need 16 bits)
	closest_dtype_bits = 2 ** math.ceil(math.log(bits, 2))
	dtype_bit_difference = closest_dtype_bits - bits
	if dtype_bit_difference > 0:
		sign_extend = np.tile(data[:,:,:, bits-1:bits], [1,1,1,dtype_bit_difference])
		data = np.concatenate([data, sign_extend], axis=-1)
	
	# now reshape it back into bytes so that we can flip the bytes around
	# (when we flipped the bytes before we actually made them backwards)
	data = np.reshape(data, [-1,8])
	data = np.flip(data, -1)

	# now we can pack them into bytes
	data = np.packbits(data, axis=-1)
	data = data.tobytes()

	# pack them into the smallest data type that is greater than or equal to the
	# bits we have
	closest_dtype = np.byte
	if closest_dtype_bits == 16:
		closest_dtype = np.short
	if closest_dtype_bits == 32:
		closest_dtype = np.int

	# converts it from an array of bytes into the datatype we found
	data = np.frombuffer(data, dtype=closest_dtype)

	# Now reshape it so that the inner dimension is a full output
	data = data.reshape([-1, values_per_transfer * total_transfers_per_output])

	# now trim any padding that was in the final PCIe transfer
	data = data[:, :output_size]

	#data = data.reshape([-1] + [dims[k] for k in ["h", "c", "w"]])
	#data = np.transpose(data, [0, 2,1,3])
	#predictions = data[0:1]
	predictions = data.reshape([-1, output_size])
	return predictions

#with open("generated_files/layer_images/verilog/verilator_read_out.bin", "rb") as fh:
#	predictions = load_bin_predicions(fh)

#with open("mobilenet_v2_from_pcie_2.bin", "rb") as fh:
#with open("resnet_50_low_dsp_validation.bin", "rb") as fh:
#with open("resnet_50_800_dsp.bin", "rb") as fh:
#with open("resnet_50_800_dsp_simulation_works_auto_bit_spec.bin", "rb") as fh:
#with open("resnet_50_800_dsp_simulation_works_custom_bit_spec.bin", "rb") as fh:
#with open("resnet_50_5000_dsp_slow_nondeterminism.bin", "rb") as fh:
#with open("resnet_50_800_dsp_old_clock_architecture.bin", "rb") as fh:
#with open("mobilenet_v1_800_dsp_old_clock_architecture.bin", "rb") as fh:
#with open("resnet_50_800_dsp_old_clock_architecture_test_2.bin", "rb") as fh:
#with open("resnet_50_800_dsp_old_clock_architecture_pcie_refclk.bin", "rb") as fh:
#with open("resnet_50_patch.bin", "rb") as fh:
#with open("resnet_50_patch_800_dsp.bin", "rb") as fh:
with open("from_pcie.bin", "rb") as fh:
	predictions = load_bin_predicions(fh)

"""channels = predictions.shape[2]
print(predictions.shape)
predictions = np.reshape(predictions, [predictions.shape[0] *predictions.shape[1] * predictions.shape[2], predictions.shape[3]])
with open("generated_files/layer_images/verilog/mobilenet_l1_from_pcie.csv", "w") as fh:
	fh.write(str(channels) + "\n")
	for i in range(0):
		fh.write(", ".join(["0" for _ in predictions[0]]) + "\n")
	for prediction in predictions:
		fh.write(", ".join([str(p) for p in list(prediction)]) + "\n")
sys.exit(0)"""

#with open("generated_files/layer_images/verilog/verilator_read_out.bin", "rb") as fh:
#	predictions = load_bin_predicions(fh)

"""
	lines = [l for l in fh]
	lines = []
	for i,l in enumerate(fh):
		lines.append(l)
		if i > 100:
			break
	classes_count = int(lines[0])
	lines = lines[1:]
	predictions = [[int(i) for i in l.split(",")][:classes_count] for l in lines]
	predictions = np.array(predictions)
	print(predictions[0])
	lower = predictions[:,::2]
	upper = predictions[:,1::2]
	lower = np.bitwise_and(lower, 0xFF)
	upper = np.left_shift(upper, 8)
	predictions = np.bitwise_or(lower, upper).astype(int)
	"""

with open("tensorflow_predictions.csv", "r") as fh:
	lines = [l for l in fh]
	tf_predictions = [[float(i) for i in l.split(",")[:-1]] for l in lines]
	tf_predictions = np.array(tf_predictions)

"""with open("verilog_images/bias_add_49.csv", "r") as fh:
	lines = [l for l in fh]
	lines = lines[1:]
	lines = lines[:1001*(len(lines)//1001)]
	sim_predictions = np.array([int(l) for l in lines])
	sim_predictions = np.reshape(sim_predictions, [-1, 1001])
"""
count = min(tf_predictions.shape[0], predictions.shape[0]) # sim_predictions.shape[0], 
predictions = predictions[:count, :]
tf_predictions = tf_predictions[:count, :]
#sim_predictions = sim_predictions[:count, :]

class_orders = list(np.argsort(predictions, axis=1))
tf_class_orders = list(np.argsort(tf_predictions, axis=1))



#sim_class_orders = list(np.argsort(sim_predictions, axis=1))

top_5_correct = 0
top_1_correct = 0
tf_top_5_correct = 0
tf_top_1_correct = 0
sim_top_5_correct = 0
sim_top_1_correct = 0

tf_top_1_hardware_not_top_5 = []
top_5_accuracy_over_time = []
top_1_accuracy_over_time = []
for i,(tf_pred, pred,c) in enumerate(zip(tf_class_orders, class_orders, classes)): # , sim_pred ... , sim_class_orders
	top_5 = pred[-5:]
	top_1 = pred[-1]
	tf_top_5 = tf_pred[-5:]
	tf_top_1 = tf_pred[-1]
	#sim_top_5 = sim_pred[-5:]
	#sim_top_1 = sim_pred[-1:]

	print(str(i) + ": " + str(c))
	print(str(top_5) + " " +  str(predictions[i][top_5]))
	print(str(tf_top_5) + " " +  str(tf_predictions[i][tf_top_5]))
	#print(str(sim_top_5) + " " +  str(sim_predictions[i][sim_top_5]))
	print()
	hw_top_5 = False
	if c in top_5:
		top_5_correct += 1
		hw_top_5 = True
	if top_1 == c:
		top_1_correct += 1
	if c in tf_top_5:
		tf_top_5_correct += 1
	if tf_top_1 == c:
		tf_top_1_correct += 1
		if not hw_top_5:
			tf_top_1_hardware_not_top_5.append(i)
	"""if c in sim_top_5:
		sim_top_5_correct += 1
	if sim_top_1 == c:
		sim_top_1_correct += 1"""

	top_5_accuracy_over_time.append(top_5_correct / float(i+1))
	top_1_accuracy_over_time.append(top_1_correct / float(i+1))

i+=1
print("Top 1 Accuracy: " + str(float(top_1_correct) / i))
print("Top 5 Accuracy: " + str(float(top_5_correct) / i))

print("TF Top 1 Accuracy: " + str(float(tf_top_1_correct) / i))
print("TF Top 5 Accuracy: " + str(float(tf_top_5_correct) / i))

#print(tf_top_1_hardware_not_top_5)

#print("Sim Top 1 Accuracy: " + str(float(sim_top_1_correct) / i))
#print("Sim Top 5 Accuracy: " + str(float(sim_top_5_correct) / i))

#plt.plot(top_5_accuracy_over_time)
#plt.plot(top_1_accuracy_over_time)
#plt.show()
