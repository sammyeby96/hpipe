from tf_graph_to_scnn import get_image_from_3d_activation
from matplotlib import pyplot as plt
import seaborn as sns
import numpy as np
import compare_output_distributions as cod
#from scipy.misc import imsave
from os import walk
import re

files = []
for (_, _, filenames) in walk("verilog"):
	for f in filenames:
		if ".csv" not in f or "basic_conv_0" not in f:
			continue
		files.append(f)
file_number_re = re.compile(r"_(\d+)\.csv")
file_number_pairs = [[file_number_re.search(f), f] for f in files]
for i,p in enumerate(file_number_pairs):
	if not p[0]:
		file_number_pairs[i][0] = -1
	else:
		file_number_pairs[i][0] = int(p[0].group(1))
file_number_pairs = sorted(file_number_pairs, key=lambda x: x[0])
files = [p[1] for p in file_number_pairs]

for f in files:
	with open("verilog/" + f, "r") as fh:
		values = [[v for v in l.split(",")] for l in fh]
	if len(values) == 0:
		print("No contents in " + f)
		continue
	channels = int(values[0][0])
	del values[0]
	if len(values) == 0:
		print("No contents in " + f)
		continue

	with open("tensorflow/" + f, "r") as fh:
		golden_values = [[int(v) for v in l.split(",")] for l in fh]
	del golden_values[0]

	length = len(values[0])
	max_height = length
	if len(values[-1]) != length:
		del values[-1]
	try:
		for i,l in enumerate(values):
			for j,v in enumerate(l):
				if "x" in v:
					values[i][j] = 0.0
				else:
					try:
						values[i][j] = int(v)
					except Exception as e:
						if i == (len(values) - 1):
							values[i][j] = 0.0
						else:
							raise e

	except Exception as e:
		print("Couldn't convert '" + str(values[i][j] + "' to float at (i,j) (" + str(i) + "," + str(j) + "), skipping " + f + "..."))
		continue
		#raise e
					

	"""
	for i,(vl,gl) in enumerate(zip(values, golden_values)):
		breaking = False
		for j,(v,g) in enumerate(zip(vl,gl)):
			if abs(v - g) > 150:
				print("Expected " + str(g) + " got " + str(v) + " in line " + str(i) + " at position " + str(j))
				breaking = True
				break
		if breaking:
			break
	"""


	"""
	np_values = np.array(values)
	np_golden = np.array(golden_values)
	np_golden = np_golden[:np_values.shape[0]]
	values = np_values - np_golden
	values = list(values)
	"""

	#values = golden_values

	print("Generating " + f)
	height = len(values) // channels
	height = min(height, max_height)
	width = len(values[0])
	values = np.array(values[:channels * height])
	values = np.reshape(values, [1, height, channels, width])
	values = np.transpose(values, [0, 1, 3, 2])
	a = np.reshape(values, [height, width, channels])

	bit_fractions = []
	print(a.dtype)
	for i in range(32):
		bit = np.right_shift(a, i) & 0x1
		#print(bit)
		total = np.sum(bit)
		fraction = total / float(a.size)
		bit_fractions.append((i,fraction))

	print(bit_fractions)
