import hpipe_ext

"""
mlperf backend
"""

# pylint: disable=unused-argument,missing-docstring,useless-super-delegation
import sys

# Modify to path to mlperf as needed
sys.path.insert(1, '/home/marius/Documents/HPIPE/mlperf/mlperf_inference/vision/classification_and_detection/python')
#sys.path.insert(1, '/home/stanmari/MLperf_test/mlperf_inference/vision/classification_and_detection/python') 

import backend

class BackendHpipe(backend.Backend):
	imagecount = 0
	HW_class_to_Imagenet_class = [0]
	accuracy_check_initialized = False

	def __init__(self):
		super(BackendHpipe, self).__init__()

	def version(self):
		return "1.0"

	def name(self):
		return "hpipe"
	
	def init_accuracy_check(self):
		with open("HW_to_ImageNet_mapping.txt", 'r') as fh:
			for line in fh:
				line = line.split(' ')
				self.HW_class_to_Imagenet_class.append(int(line[1]))
		self.accuracy_check_initialized = True

	def image_format(self):
		# Not sure if we need this since our inputs are a custom bin
		return "NHWC"

	def load(self, model_path, inputs=None, outputs=None):
		print("\n IMPORTANT!")
		print("Make sure to setup the accelerator on the FPGA by referencing the first part of https://mathewkhall.gitlab.io/hpipe/usage/quickstart.html#running-the-accelerator")
		
		return self

	def predict(self, image_id, take_accuracy):

		if isinstance(image_id, list): 
			if len(image_id) != 1:
				print("ERROR: We currently only support singlestream for hpipe")
				return None
			image_id = image_id[0]
			
		
		result = [[hpipe_ext.hpipe_main(image_id)]] # IDK what the correct format of the result is but this seems to work
		
		# This code is only for checking accuracy during preliminary testing. It will not be used in performance mode or for the final accuracy test (very slow)
		if take_accuracy:
			import operator
			from compare_classes_to_hardware import load_bin_predicions
			predictions = None
			self.imagecount += 1
			with open("from_pcie.bin", "rb") as fh:
				predictions = load_bin_predicions(fh)
			
			predicted_class, value = max(enumerate(predictions[0]), key=operator.itemgetter(1))
			if not self.accuracy_check_initialized: self.init_accuracy_check()
			
			result = [[self.HW_class_to_Imagenet_class[predicted_class] + 1]]  #Add 1 since classes should technically start at 0 for imagenet
			#with open("output_classes.txt", 'a') as fh:
			#	fh.write(str(image_id) + ' ' + str(result[0][0]) + '\n')

			print("Class predicted for image ID " + str(image_id) + " is " + str(result[0][0]) + " (total tests = " + str(self.imagecount) + ')')
		
		
		return result	
