import numpy as np
import sys
from IABuffer import IABuffer, IABufferModel
from SparseWeights import SparseWeights
from Accumulator import Accumulator, AccumulatorModel
from WeightBinner import WeightBinner

def main():
	np.random.seed(0)
	max_cycles = 3000000
	requesters = 1
	ia_shape = (1,10,10,3)
	kernel_shape = (1,1,3,64)
	parallel_reduction = 2
	stride = 2
	padding = "same"
	p_request = 1
	request_pattern_length = 100
	n_buffering = 1 # only 1 supported at the moment
	bins = 1

	weight_count = np.prod(kernel_shape)
	weights = (np.reshape(np.random.binomial(1, 0.15, weight_count), kernel_shape) * np.random.normal(0.,1.,kernel_shape) * 20).astype(int)

	nonzero_oc_group_count = np.nonzero(np.sum(weights, axis=(0,1,2)))[0].size
	collected_oc_count = 0

	sparse_weights = SparseWeights(weights)
	binner = WeightBinner(bins, sparse_weights)
	accumulators = []
	accumulator_models = []
	for i in range(bins):
		accumulators.append(Accumulator(kernel_shape, 3, 2, parallel_reduction))
		for swd in reversed(binner.bins[i]):
			accumulators[-1].add_oc_group(*swd)
		accumulator_models.append(AccumulatorModel(accumulators[-1], 1))

	ic_orders = np.random.permutation(ia_shape[-1])
	ics_per_requester = ia_shape[-1] // requesters
	ics_for_requesters = [ics_per_requester for _ in range(requesters)]
	extra_ics = ia_shape[-1] - ics_per_requester * requesters
	for i in range(extra_ics):
		ics_per_requester[i] += 1
	ic_orders_by_requester = []
	ic_index = 0
	for ics in ics_for_requesters:
		ic_orders_by_requester.append(ic_orders[ic_index:ic_index+ics])
		ic_index += ics

	cycle_count = 0
	request_pattern = np.reshape(np.random.binomial(1, p_request, request_pattern_length * requesters), [request_pattern_length, requesters])
	pending_requests = np.array([False for _ in range(requesters)])
	requester_indices = np.array([0 for _ in range(requesters)])
	requester_end_indices = np.array([o.shape[-1] for o in ic_orders_by_requester])
	done = lambda: requester_indices == requester_end_indices
	all_done = lambda: done().all()
	requests = lambda g: ((np.invert(g)) and pending_requests) or (request_pattern[(cycle_count % request_pattern_length)] and np.invert(done()))

	ia = (np.random.normal(0., 1., ia_shape) * 20).astype(int)
	ia_buffer = IABuffer(ia_shape, kernel_shape, padding, stride, requesters, n_buffering)
	golden_oa = np.zeros(ia_buffer.output_spatial_shape + [kernel_shape[-1]])
	computed_oa = np.zeros(ia_buffer.output_spatial_shape + [kernel_shape[-1]])
	padded_ia = np.pad(ia, ia_buffer.padding, "constant", constant_values=0)
	for i in range(golden_oa.shape[0]):
		for j in range(golden_oa.shape[1]):
			golden_oa[i,j,:] = np.sum(weights * padded_ia[0,i*stride:i*stride+kernel_shape[0],j*stride:j*stride+kernel_shape[1],:,np.newaxis], axis=(0,1,2))
	buffer_model = IABufferModel(ia_buffer)

	oa_idx = 0
	oy = lambda: oa_idx // golden_oa.shape[0]
	ox = lambda: oa_idx % golden_oa.shape[1]

	ia_idx = 0
	iy = lambda: ia_idx // ia_shape[1]
	ix = lambda: ia_idx % ia_shape[2]

	end_oa_idx = np.prod(golden_oa.shape)
	end_ia_idx = np.prod(ia_shape[1:3]) - 1
	kernel_collection = np.zeros(np.prod(kernel_shape[:3]))
	finished = False
	reqs = [[False] for i in range(bins)]
	total_req_count = 0
	while cycle_count < max_cycles and oa_idx != end_oa_idx:
		already_granted = False
		req_count = 0
		channel_indices = []
		d = done()
		for i,cs in enumerate(ic_orders_by_requester):
			if d[i]:
				channel_indices.append(0)
				continue
			channel_indices.append(cs[requester_indices[i]])
		data = ia[0,iy(),ix(),channel_indices]
		outputs = buffer_model.step(pending_requests, data, channel_indices, all_done() and not finished, [0 for _ in range(requesters)], False)
		for i,m in enumerate(accumulator_models):
			granting = reqs[i][0] and not already_granted
			if reqs[i][0] and already_granted:
				total_req_count += 1
			accum_outputs = m.step(outputs["valid"], outputs["addr"], outputs["data"], [granting])
			reqs[i] = accum_outputs["reqs"]
			if granting:
				already_granted = True
				computed_oa[oy(), ox(), accum_outputs["oc"]] = accum_outputs["data"]
				if computed_oa[oy(), ox(), accum_outputs["oc"]] != golden_oa[oy(), ox(), accum_outputs["oc"]]:
					print(accum_outputs["oc"])
					print(oy())
					print(ox())
					_indices = np.nonzero(weights[:,:,:,accum_outputs["oc"]])
					print(weights[:,:,:,accum_outputs["oc"]])
					print(padded_ia[0,oy()*stride:oy()*stride+kernel_shape[0],ox()*stride:ox()*stride+kernel_shape[1], :])
					print(np.transpose(_indices))
					print(weights[:,:,:,accum_outputs["oc"]][_indices])
					print(padded_ia[0,oy()*stride:oy()*stride+kernel_shape[0],ox()*stride:ox()*stride+kernel_shape[1], :][_indices])
					print(computed_oa[oy(), ox(), accum_outputs["oc"]])
					print(golden_oa[oy(), ox(), accum_outputs["oc"]])
					assert(computed_oa[oy(), ox(), accum_outputs["oc"]] == golden_oa[oy(), ox(), accum_outputs["oc"]])
		if all_done():
			if ia_idx != (end_ia_idx):
				ia_idx += 1
				requester_indices.fill(0)
			else:
				finished = True

		gnt_indices = np.nonzero(outputs["gnts"])
		requester_indices[gnt_indices] += 1
		pending_requests = requests(outputs["gnts"])

		if already_granted:
			collected_oc_count += 1
		if already_granted and collected_oc_count == nonzero_oc_group_count:
			oa_idx += 1
			collected_oc_count = 0
		cycle_count += 1
	if cycle_count >= max_cycles:
		print("Reached max cycle count")
		print("ia_idx: " + str(ia_idx))
		print("oa_idx: " + str(oa_idx))
		sys.exit(-1)
	print(computed_oa)
	print(golden_oa)
	print((computed_oa == golden_oa))
	assert((computed_oa == golden_oa).all())

"""	accumulators = []
	accumulator_models = []
	for i in range(bins):
		accumulators.append(Accumulator(kernel_shape, 3, 2, parallel_reduction))
		for swd in reversed(binner.bins[i]):
			accumulators[-1].add_oc_group(*swd)
		accumulator_models.append(AccumulatorModel(accumulators[-1], 1))

	collected_oa = np.zeros_like(golden_oa)
	collected    = np.zeros_like(golden_oa)
	flat_ia = ia.flatten()
	reqs = [[False] for i in range(bins)]
	cycle_count = 0
	total_req_count = 0
	while cycle_count < max_cycles and collected.size != np.sum(collected):
		already_granted = False
		valid = False
		v = 0
		addr = 0
		req_count = 0
		if cycle_count < flat_ia.size:
			valid = True
			v = flat_ia[cycle_count]
			addr = cycle_count
		cycle_count += 1
		for i,m in enumerate(accumulator_models):
			granting = reqs[i][0] and not already_granted
			if reqs[i][0] and already_granted:
				total_req_count += 1
			outputs = m.step(valid, addr, v, [granting])
			reqs[i] = outputs["reqs"]
			if granting:
				already_granted = True
				collected[outputs["oc"]] = 1
				collected_oa[outputs["oc"]] = outputs["data"]
				indices = list(np.nonzero(weights[:,:,:,outputs["oc"]:outputs["oc"]+1]))
				acts = ia[indices]
				indices[-1] += outputs["oc"]
				ws = weights[indices]
				assert(collected_oa[outputs["oc"]] == golden_oa[outputs["oc"]])
	print(collected_oa == golden_oa)
	print(cycle_count)
	print(float(total_req_count) / float(cycle_count))
	assert((collected_oa == golden_oa).all())"""



if __name__ == "__main__":
	main()