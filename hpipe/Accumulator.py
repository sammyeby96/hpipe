import numpy as np
import math
import digital_logic as dl
from copy import copy
#from print_act_buffer_allocation import get_lines_in_file

class Accumulator():
	def __init__(self, layer_weight_shape, oc_index, ic_index, parallel_reduction=1):
		self.oc_groups = []
		self.layer_indices = []
		self.layer_weight_shape = layer_weight_shape
		self.oc_index = oc_index
		self.ic_index = ic_index
		self.parallel_reduction = parallel_reduction

	def get_no_oc_select(self):
		no_oc_select = list(range(len(self.layer_weight_shape)))
		del no_oc_select[self.oc_index]
		return no_oc_select

	def add_oc_group(self, weights, indices):
		padding = self.parallel_reduction * math.ceil(weights.size / float(self.parallel_reduction)) - weights.size
		weights = np.pad(weights, (0, padding), "constant", constant_values=(0,0))
		indices = np.pad(indices, ((0, padding),(0,0)), "reflect")
		self.oc_groups.append(weights)
		self.layer_indices.append(indices)

	def _get_unique_input_indices(self):
		return np.unique(np.concatenate(
			self.layer_indices, axis=0)[:, self.get_no_oc_select()], axis=0)

	def _get_addresses_from_indices(self, indices):
		def get_index_multipliers():
			no_oc_select = np.array(self.get_no_oc_select())
			index_multipliers = []
			cumulative_multiplier = 1
			for i in reversed(no_oc_select):
				index_multipliers.append(cumulative_multiplier)
				cumulative_multiplier *= self.layer_weight_shape[i]
			return list(reversed(index_multipliers))

		index_multipliers = np.expand_dims(np.array(get_index_multipliers()), 0)
		pre_reduction_indices = indices * index_multipliers
		return np.sum(pre_reduction_indices, axis=1)


	def get_layer_addresses(self):
		indices = self._get_unique_input_indices()
		addresses = self._get_addresses_from_indices(indices)
		return np.sort(addresses)

	def get_oc_group_local_indices(self):
		sorted_unique_addresses = self.get_layer_addresses()
		oc_groups_addresses = [self._get_addresses_from_indices(
			indices[:,self.get_no_oc_select()]) for indices in self.layer_indices]
		def get_indices_of_oc_group_addresses(oc_group_addresses):
			index = 0
			_indices = oc_group_addresses.copy()
			sorted_indices = np.argsort(oc_group_addresses)
			for i in sorted_indices:
				addr = oc_group_addresses[i]
				while sorted_unique_addresses[index] != addr:
					index += 1
				_indices[i] = index
			return _indices
		indices = []
		for oc_group_addrs in oc_groups_addresses:
			indices.extend(get_indices_of_oc_group_addresses(oc_group_addrs))

		return np.reshape(np.array(indices), [-1, self.parallel_reduction])

	def get_oc_group_count(self):
		return len(self.oc_groups)

	def get_oc_group_accum_counts(self):
		return [math.ceil(w.size / float(self.parallel_reduction)) for w in self.oc_groups]

	def get_max_oc_group_accum_count(self):
		return max(self.get_oc_group_accum_counts())

	def get_output_indices(self):
		return [indices[0,self.oc_index] for indices in self.layer_indices]

	def get_weights(self):
		return np.reshape(np.concatenate(self.oc_groups, axis=0), [-1, self.parallel_reduction])

class AccumulatorModule(dl.Module):
	def __init__(self, accumulator, kind="accumulator_module", **kwargs):
		super(AccumulatorModule, self).__init__("accumulator_module", kind=kind, **kwargs)
		kwargs = self.kwargs




class AccumulatorModel():
	def __init__(self, accumulator, num_outputs):
		self.num_outputs           = num_outputs
		self.addresses             = accumulator.get_layer_addresses()
		self.address_count         = self.addresses.size
		self.address_count_m1      = self.address_count - 1
		self.act_buffer_size       = 2 ** math.ceil(math.log2(self.address_count))
		self.oc_group_count        = accumulator.get_oc_group_count()
		self.oc_group_count_m1     = self.oc_group_count - 1
		self.oc_group_accum_counts = np.array(accumulator.get_oc_group_accum_counts()) - 1
		self.weights               = accumulator.get_weights()
		self.local_indices         = accumulator.get_oc_group_local_indices()
		self.oc_indices            = accumulator.get_output_indices()

		assert(len(self.oc_indices) == len(self.oc_group_accum_counts))
		assert(len(self.weights)    == len(self.local_indices))

		self.processing_buffer   = [False, False]
		self.read_buffer         = 0 # 0 or 1 for double buffering
		self.write_buffer        = 0 # 0 or 1 for double buffering
		self.write_address       = 0
		self.weight_address      = 0
		self.oc_counter          = 0
		self.act_buffer          = np.zeros(self.act_buffer_size*2, dtype=int)
		self.requests            = [False for i in range(num_outputs)]
		self.req_data            = None
		self.req_oc              = None
		self.partial_sum         = 0
		self.input_group_counter = self.oc_group_accum_counts[0]
		self.can_accumulate_new_data = True
		self.ia_history = []
		self.weight_history = []
		self.product_history = []

	def step(self, valid, addr, data, write_gnts):
		rvalue = {
			"reqs" : copy(self.requests),
			"data" : copy(self.req_data),
			"oc"   : copy(self.req_oc),
			"wait" : copy(self.processing_buffer[self.write_buffer])
		}
		self.requests = [r and not g for r,g in zip(self.requests, write_gnts)]
		if self.processing_buffer[self.read_buffer]:
			self._step_buffer()
		if valid:
			self._step_gather(addr, data)
		rvalue["reqs"] = self.requests
		return rvalue

	def _try_to_make_new_request(self):
		self.can_accumulate_new_data = False
		for req in self.requests:
			if req:
				return
		self.can_accumulate_new_data = True
		self.req_data = self.partial_sum
		self.req_oc = self.oc_indices[self.oc_counter]
		self.requests = [True for _ in self.requests]
		self.partial_sum = 0

		self.req_ia_history = self.ia_history
		self.req_weight_history = self.weight_history
		self.req_product_history = self.product_history

		self.ia_history = []
		self.weight_history = []
		self.product_history = []

		if self.oc_counter == self.oc_group_count_m1:
			self.oc_counter          = 0
			self.weight_address      = 0
			self.input_group_counter = self.oc_group_accum_counts[0]
			if self.read_buffer == 0:
				self.read_buffer = 1
			else:
				self.read_buffer = 0
		else:
			self.oc_counter += 1
			self.input_group_counter = self.oc_group_accum_counts[self.oc_counter]
		return


	def _step_buffer(self):
		if not self.can_accumulate_new_data:
			return self._try_to_make_new_request()
		ws = self.weights[self.weight_address]
		acts = self.act_buffer[self.local_indices[self.weight_address] * 2 + self.read_buffer]
		self.weight_history.extend(ws)
		self.ia_history.extend(acts)
		product = ws * acts
		self.product_history.extend(product)
		self.partial_sum += np.sum(product)
		self.weight_address += 1
		if self.input_group_counter == 0:
			self._try_to_make_new_request()
		else:
			self.input_group_counter -= 1


	def _step_gather(self, addr, data):
		if addr in self.addresses:
			self.act_buffer[self.write_address * 2 + self.write_buffer] = data
			if self.write_address == self.address_count_m1:
				self.write_address = 0
				self.processing_buffer[self.write_buffer] = True
				if self.write_buffer == 0:
					self.write_buffer = 1
				else:
					self.write_buffer = 0
			else:
				self.write_address += 1


