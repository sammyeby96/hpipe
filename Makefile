CIRCUITS_DIR=generated_files/circuits
CIRCUIT_DIR=generated_circuit
QUARTUS_CIRCUIT_DIR=quartus_circuit
ARG_MAX=$(shell getconf ARG_MAX)
VCS_WORK_DIR=work
VCS_OUTPUT_NAME=simv
VERILATOR_DEFINES=+define+WRITE_IMAGES+1
TOP=hpipe_tb_0
VERILATOR_TOP=hpipe_0
VERILATOR_MAIN=quartus_component_verilator_top.cpp
VCS_COMPILE_DIR=sim/vcs/compile
VERILATOR_OUTPUT_DIR=sim/verilator/compile
ROOT=$(shell pwd)
CIRCUIT_PATH=$(ROOT)/$(CIRCUITS_DIR)/$(CIRCUIT_DIR)
QUARTUS_CIRCUIT_PATH=$(ROOT)/$(CIRCUITS_DIR)/$(QUARTUS_CIRCUIT_DIR)
VERILATOR_MAIN_PATH=$(ROOT)/$(VERILATOR_MAIN)
XPROP_DIR=xprop
XPROP_PATH=$(ROOT)/$(CIRCUITS_DIR)/$(XPROP_DIR)
OBJCACHE=ccache
XINITIAL=fast
XASSIGN=fast
VERILATOR_THREADS:=$(shell lscpu --online --parse=Core,Socket | grep ',0' | sort -u | wc -l)

QUARTUS_SIM_LIB_ARGS= +cli+1 -line -timescale=1ps/1ps -sverilog \
	-v /home/mat/drive2/intelFPGA_pro/18.1/quartus/eda/sim_lib/altera_primitives.v \
	-v /home/mat/drive2/intelFPGA_pro/18.1/quartus/eda/sim_lib/220model.v \
	-v /home/mat/drive2/intelFPGA_pro/18.1/quartus/eda/sim_lib/sgate.v \
	-v /home/mat/drive2/intelFPGA_pro/18.1/quartus/eda/sim_lib/altera_mf.v \
	-v /home/mat/drive2/intelFPGA_pro/18.1/quartus/eda/sim_lib/altera_lnsim.sv \
	-v /home/mat/drive2/intelFPGA_pro/18.1/quartus/eda/sim_lib/fourteennm_atoms.sv \
	-v /home/mat/drive2/intelFPGA_pro/18.1/quartus/eda/sim_lib/synopsys/fourteennm_atoms_ncrypt.sv \
	-v /home/mat/drive2/intelFPGA_pro/18.1/quartus/eda/sim_lib/ct1_hssi_atoms.sv \
	-v /home/mat/drive2/intelFPGA_pro/18.1/quartus/eda/sim_lib/synopsys/ct1_hssi_atoms_ncrypt.sv \
	-v /home/mat/drive2/intelFPGA_pro/18.1/quartus/eda/sim_lib/synopsys/cr3v0_serdes_models_ncrypt.sv \
	-v /home/mat/drive2/intelFPGA_pro/18.1/quartus/eda/sim_lib/ct1_hip_atoms.sv \
	-v /home/mat/drive2/intelFPGA_pro/18.1/quartus/eda/sim_lib/synopsys/ct1_hip_atoms_ncrypt.sv \
	 +incdir+/home/mat/drive2/intelFPGA_pro/18.1/quartus/eda/sim_lib

.PHONY: xprop quartus_project_include_list
all:
	mkdir -p $(VCS_COMPILE_DIR)
	#+define+DONT_ALLOW_X_WRITE_TO_RAM+1
	-cd $(VCS_COMPILE_DIR); \
	rm $(CIRCUIT_DIR) memory_initialization_files generated_files binary_inputs; \
	ln -s $(CIRCUIT_PATH) $(CIRCUIT_DIR); \
	ln -s $(ROOT)/generated_files generated_files; \
	ln -s $(CIRCUIT_PATH)/memory_initialization_files memory_initialization_files; \
	ln -s $(ROOT)/generated_files/binary_inputs binary_inputs
	cd $(VCS_COMPILE_DIR); \
	time vcs $(QUARTUS_SIM_LIB_ARGS) -CFLAGS "-O3" -O4 -lca -ld /usr/bin/g++-4.8 +define+TESTING+1 +define+DISPLAY_FINISH_TIMES+1 +define+WRITE_IMAGES+1 -f $(CIRCUIT_PATH)/verilog_paths.txt -y $(CIRCUIT_PATH) -work $(VCS_WORK_DIR) -full64 -top hpipe_tb_0 -sverilog -o $(VCS_OUTPUT_NAME); \
	time ./$(VCS_OUTPUT_NAME)

dump:
	mkdir -p $(VCS_COMPILE_DIR)
	-cd $(VCS_COMPILE_DIR); \
	rm $(CIRCUIT_DIR) memory_initialization_files generated_files binary_inputs; \
	ln -s $(CIRCUIT_PATH) $(CIRCUIT_DIR); \
	ln -s $(ROOT)/generated_files generated_files; \
	ln -s $(CIRCUIT_PATH)/memory_initialization_files memory_initialization_files; \
	ln -s $(ROOT)/generated_files/binary_inputs binary_inputs
	cd $(VCS_COMPILE_DIR); \
	time vlogan -CFLAGS "-O3" -O4 +define+DUMP+1 +define+TESTING+1 +define+WRITE_IMAGES+1  +define+DISPLAY_FINISH_TIMES+1 -f $(CIRCUIT_PATH)/verilog_paths.txt -y $(CIRCUIT_PATH) -work $(VCS_WORK_DIR) $(QUARTUS_SIM_LIB_ARGS) -debug_pp -debug_all +vcs+vcdpluson -full64 -sverilog; \
	time vcs -CFLAGS "-O3" -O4 -ld /usr/bin/g++-4.8 +vcs+lic+wait -y $(CIRCUIT_PATH)  -debug_pp -debug_all +vpdfilesize+2000 +vcs+vcdpluson -full64 hpipe_tb_0 -o $(VCS_OUTPUT_NAME); \
	time ./$(VCS_OUTPUT_NAME)

quartus_project_include_list:
	./build_utils/make_quartus_include_list.sh $(QUARTUS_CIRCUIT_DIR) $(QUARTUS_CIRCUIT_PATH)

quartus_project: quartus_project_include_list
	cd $(QUARTUS_CIRCUIT_PATH); \
	tar -xzf $(ROOT)/quartus_project.tgz

circuit:
	CUDA_VISIBLE_DEVICES= python3 -m hpipe.LayerComponents

quartus:
	CUDA_VISIBLE_DEVICES= python3 -m hpipe.LayerComponents --param_file_path "quartus_params.json" --quartus

quartus_nx:
	CUDA_VISIBLE_DEVICES=0 python3 -m hpipe.LayerComponents --param_file_path "params_file.json" --quartus --arch architectures/s10_NX.json --device devices/s10_NX.json
	./build_utils/make_quartus_include_list.sh $(QUARTUS_CIRCUIT_DIR) $(QUARTUS_CIRCUIT_PATH)

tanh_test:
	CUDA_VISIBLE_DEVICES= python3 -m hpipe.LayerComponents --param_file_path "tanh_params_file.json" --quartus

cleanup:
	./build_utils/cleanup_circuit_dir.sh $(CIRCUIT_PATH)

cleanup_quartus:
	./build_utils/cleanup_circuit_dir.sh $(QUARTUS_CIRCUIT_PATH)

verilator_run:
	cd $(VERILATOR_OUTPUT_DIR); \
	./verilator_sim

verilator: verilator_verilog
	# --trace-fst
	#  -DDUMP=1
	make VM_PARALLEL_BUILDS=1 OBJCACHE=$(OBJCACHE) OPT_FAST=" -O1 -fno-stack-protector" -j $(VERILATOR_THREADS) -C $(VERILATOR_OUTPUT_DIR) -f V$(VERILATOR_TOP).mk
	# verilator doesn't support search paths for $readmemh, so I just create a simlink
	# to the top level source dir in the verilator output directory and cd to that dir
	# before running verilator
	-cd $(VERILATOR_OUTPUT_DIR); \
	rm $(QUARTUS_DIR) memory_initialization_files generated_files binary_inputs; \
	ln -s $(QUARTUS_CIRCUIT_PATH) $(QUARTUS_DIR); \
	ln -s $(QUARTUS_CIRCUIT_PATH)/memory_initialization_files memory_initialization_files; \
	ln -s $(ROOT)/generated_files generated_files; \
	ln -s $(ROOT)/generated_files/binary_inputs binary_inputs
	#

verilator_verilog: $(QUARTUS_CIRCUIT_PATH)/verilog_paths.txt
	# +define+WRITE_IMAGES+1 
	# --trace-fst
	mkdir -p $(VERILATOR_OUTPUT_DIR)
	verilator $(VERILATOR_DEFINES) --threads $(VERILATOR_THREADS) +define+DISPLAY_FINISH_TIMES+1 -Mdir $(VERILATOR_OUTPUT_DIR) --autoflush -O1 --x-assign $(XASSIGN) --x-initial $(XINITIAL) --noassert --top-module $(VERILATOR_TOP) -Wno-fatal -o verilator_sim --cc -y $(ROOT)/$(CIRCUITS_DIR) -f $(QUARTUS_CIRCUIT_PATH)/verilog_paths.txt --exe $(VERILATOR_MAIN_PATH)

xprop:
	mkdir -p $(XPROP_PATH)
	./build_utils/make_xprop.sh $(CIRCUIT_PATH) $(XPROP_PATH)
	cp -r $(CIRCUIT_PATH)/memory_initialization_files $(XPROP_PATH)
