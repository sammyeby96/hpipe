import numpy as np
import math
from PIL import Image
import argparse
import os

#_R_MEAN = 123
#_G_MEAN = 116
#_B_MEAN = 103
#_CHANNEL_MEANS = [_R_MEAN, _G_MEAN, _B_MEAN]
_CHANNEL_MEANS = [0, 0, 0]

parser = argparse.ArgumentParser(description='Dump an image from an input bin file')
parser.add_argument('--bin_file_path', required=True, dest='bin_path',
                   type=str,
                   help='Specify a path to the bin file from which we will be dumping an image')
parser.add_argument('--output_path', required=True,
                   type=str,
                   help='Specify a path to which we should save the output png')
parser.add_argument("--image_number", type=int, default=0, help="Specify which image number you would like to dump")
parser.add_argument("--height", type=int, default=224, help="Specify the input image height")
parser.add_argument("--width", type=int, default=224, help="Specify the input image width")
args = parser.parse_args()


image_index = args.image_number
h = args.height
w = args.width
c = 3
interface_width = 256
bits_per_pixel = 8
bytes_per_transfer = interface_width // 8
pixels_per_transfer = interface_width // bits_per_pixel
pixel_bits_per_transfer = bits_per_pixel * pixels_per_transfer
padding_per_transfer = interface_width - pixel_bits_per_transfer
transfers_per_w = math.ceil(w / float(pixels_per_transfer))

required_bytes = bytes_per_transfer * transfers_per_w * h * c

bin_size = os.path.getsize(args.bin_path)
print(f"Dumping image {image_index}/{bin_size // required_bytes}")

with open(args.bin_path, "rb") as fh:
	fh.seek(image_index*required_bytes)
	fbytes = np.flip(np.fromfile(fh, dtype=np.uint8, count=required_bytes), axis=0)
bits = np.unpackbits(fbytes)
bits = np.flip(np.reshape(bits, [-1, transfers_per_w, interface_width]), axis=1)
transfer_padding_removed = np.reshape(bits, [-1, interface_width])[:,padding_per_transfer:]
padding_removed = np.reshape(transfer_padding_removed,[-1, pixel_bits_per_transfer * transfers_per_w]
	)[:, :bits_per_pixel*w]

vector_of_unpacked_bits = np.flip(np.reshape(padding_removed, [-1, bits_per_pixel]), axis=0)
sign_extend = np.repeat(vector_of_unpacked_bits[:,0:1], 16-bits_per_pixel, axis=1)
extended = np.concatenate([sign_extend, vector_of_unpacked_bits], axis=1)
packed = np.packbits(extended, axis=-1).astype(np.uint16)
img_vector = (packed[:,1:2] + np.left_shift(packed[:,0:1], 8)).astype(np.int16)
#img = np.reshape(img_vector, [h, c, 8, w//8])
#img = np.flip(img, axis=-2)
img = np.reshape(img_vector, [h, c, w])
img = np.transpose(img, [0,2,1])
#img += np.array([[_CHANNEL_MEANS]])
img = img.astype(np.uint8)
img = np.flip(img, axis=1)

(d,f) = os.path.split(args.output_path)
if len(d) != 0: os.makedirs(d, exist_ok=True)
Image.fromarray(img).convert("RGB").save(args.output_path)

