from string import Template as T

activation_count = 41
string = ""
dirs = ["verilog_images", "tensorflow_images"]

image_compare_template = T(r'''
\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{$image1}
  \caption{$caption1}
  \label{$label1}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{$image2}
  \caption{$caption2}
  \label{$label2}
\end{subfigure}
\caption{$global_caption}
\label{$global_label}
\end{figure}
''')

for i in range(activation_count):
	labels = []
	captions = []
	images = []
	for d in dirs:
		labels.append("fig:" + d.lower() + "_" + str(i) + "_conv")
		captions.append(d.replace("_"," "))
		images.append(d + "/basic_conv_"+str(i)+".png")
	global_label = "fig:basicconv" + str(i)
	global_caption = "L" + str(i) + " Convolution Output"
	string += image_compare_template.substitute(
		label1 = labels[0],
		label2 = labels[1],
		caption1 = captions[0],
		caption2 = captions[1],
		image1 = images[0],
		image2 = images[1],
		global_caption = global_caption,
		global_label = global_label
		)

print(string)
