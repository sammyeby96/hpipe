import numpy as np

class WeightBinner():
	def __init__(self, n_bins, sparse_weights):
		self.bins = [WeightBin() for _ in range(n_bins)]
		group_sizes = [wg.size for wg in sparse_weights.weight_groups]
		sorted_size_indices = np.argsort(group_sizes)[::-1]
		for i,b in enumerate(self.bins):
			index = sorted_size_indices[i]
			b.add_weights(*sparse_weights[index])

		bin_sizes = [b.get_size() for b in self.bins]
		def get_min_bin_index(bin_sizes):
			minimum = bin_sizes[0]
			index = 0
			for i,bs in enumerate(bin_sizes[1:]):
				if bs < minimum:
					minimum = bs
					index = i + 1
			return index

		for i in range(n_bins, len(group_sizes)):
			bin_index = get_min_bin_index(bin_sizes)
			weight_index = sorted_size_indices[i]
			self.bins[bin_index].add_weights(*sparse_weights[weight_index])
			bin_sizes[bin_index] = self.bins[bin_index].get_size()



class WeightBin():
	def __init__(self):
		self.weights = []
		self.indices = []
		self.size = 0

	def add_weights(self, weights, indices):
		self.weights.append(weights)
		self.indices.append(indices)
		self.size += weights.size

	def get_size(self):
		return self.size

	def __getitem__(self, i):
		return (self.weights[i], self.indices[i])

	def __iter__(self):
		return WeightBinIterator(self)

	def __len__(self):
		return len(self.weights)

class WeightBinIterator():
	def __init__(self, weight_bin):
		self.weight_bin = weight_bin
		self.current = 0
		self.upper = len(self.weight_bin.weights)

	def __next__(self):
		if self.current == self.upper:
			raise StopIteration
		else:
			self.current += 1
			return (self.weight_bin.weights[self.current-1], self.weight_bin.indices[self.current-1])
