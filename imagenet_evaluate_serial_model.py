from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os

from absl import app as absl_app
from absl import flags
import tensorflow as tf  # pylint: disable=g-bad-import-order
import sys
import getopt
import os
import re

from official.utils.flags import core as flags_core
from official.utils.logs import logger
from official.resnet_sparse import imagenet_preprocessing
from official.resnet_sparse import resnet_model
from official.resnet_sparse import imagenet_main
from official.resnet_sparse import resnet_run_loop

def model_fn(features, labels, params):

	with tf.gfile.GFile(params["graphdef_path"], "rb") as f:
		gd = tf.GraphDef()
		gd.ParseFromString(f.read())

	returned_elements = []
	input_map = {}
	for node in gd.node:
		if "input" in node.name: #"Placeholder" in node.name:
			print("Placeholder name is: " + node.name)
			print("Features input name is: " + features.name)
			input_map[node.name] = features
		else:
			returned_elements.append(node.name)
	nodes = tf.import_graph_def(gd, return_elements=returned_elements, input_map=input_map)
	for node in nodes:
		#print(node.name)
		if node.name == "import/MobilenetV1/Logits/Conv2d_1c_1x1/BiasAdd":#"import/resnet_model/final_dense":
			logits = node.outputs[0][:,0,0,:]
	predictions = {
	    'classes': tf.argmax(logits, axis=1),
	    'probabilities': tf.nn.softmax(logits, name='softmax_tensor')
	}
	print(logits.shape)
	print(labels.shape)
	accuracy = tf.metrics.accuracy(labels, predictions["classes"])
	accuracy_top_5 = tf.metrics.mean(tf.nn.in_top_k(predictions=logits,
	                                              targets=labels,
	                                              k=5,
	                                              name='top_5_op'))
	metrics = {'accuracy': accuracy,
	           'accuracy_top_5': accuracy_top_5}
	cross_entropy = tf.losses.sparse_softmax_cross_entropy(
	  logits=logits, labels=labels)

	return tf.estimator.EstimatorSpec(
	  mode=tf.estimator.ModeKeys.EVAL,
	  loss=cross_entropy,
	  predictions=predictions,
	  eval_metric_ops=metrics)



def parse_args(argv):
	try:
		opts, args = getopt.getopt(argv, "", [
			"graphdef=",
			"data_dir="])
		for i,opt in enumerate(opts):
			if len(opt) == 1:
				opt = (opt[0], 1)
				opts[i] = opt
		opts = dict(opts)
		if not "--graphdef" in opts or not "--data_dir" in opts:
			raise getopt.GetoptError
	except getopt.GetoptError:
		print("Usage: PYTHONPATH=$PYTHONPATH:/home/mat/tensorflow_models imagenet_evaluate_serial_model.py --graphdef <path/to/graphdef.pb> --data_dir <datadir>")
		sys.exit(2)
	return opts

def main(argv):
	opts = parse_args(argv)
	def input_fn_eval():
	    return imagenet_main.input_fn(
	        is_training=False, data_dir=opts["--data_dir"],
	        batch_size=128,
	        num_epochs=1)

	benchmark_logger = logger.get_benchmark_logger()
	classifier = tf.estimator.Estimator(model_fn, params={"graphdef_path":opts["--graphdef"]})
	eval_results = classifier.evaluate(input_fn=input_fn_eval)
	benchmark_logger.log_evaluation_result(eval_results)
	print(eval_results)


if __name__ == "__main__":
	main(sys.argv[1:])