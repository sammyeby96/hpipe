import numpy as np

class SparseWeights():
	# oc index must be 4, and there must be 4 input dimensions
	def __init__(self, weights):
		assert(len(weights.shape) == 4)
		self.weight_groups = []
		self.index_groups = []
		self._build_sparse_weights(weights)

	def _build_sparse_weights(self, weights):
		for oc in range(weights.shape[-1]):
			indices = list(np.nonzero(weights[:,:,:,oc:oc+1]))
			if indices[0].size == 0:
				continue
			indices[3] += oc
			w = weights[indices]
			i = np.transpose(indices)
			self.weight_groups.append(w)
			self.index_groups.append(i)

	def __getitem__(self, i):
		return (self.weight_groups[i], self.index_groups[i])

