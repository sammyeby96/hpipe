import json
import math
import os

class classproperty(object):

    def __init__(self, fget):
        self.fget = fget

    def __get__(self, owner_self, owner_cls):
        return self.fget(owner_cls)



class Architecture(object):
	"""
S10 arch spec
{
	"RAMs" : {
		"M20K" : {
			"shapes" : [[10, 2048], [20, 1024], [40, 512]],
			"output_regs" : true,
			"force_to_zero" : true,
			"multi_aspect" : true,
			"continuous_read" : false
		},
		"MLAB" : {
			"shapes" : [[20, 32]],
			"output_regs" : false,
			"force_to_zero" : false,
			"multi_aspect" : false,
			"continuous_read" : true
		}
	},
	"DSPs" : {
		"sizes" : [[32, 1], [18, 2]],
		"chaining" : true,
		"dynamic_chaining": false,
		"accumulation" : true
	}
}

S10 2800:
{
	"RAMs" : {
		"M20K" : 11520,
		"MLAB" : 46656
	},
	"DSPs" : 5760
}
	"""
	_current_arch = None
	@classproperty
	def current_arch(cls):
		if cls._current_arch is None:
			script_dir = os.path.dirname(os.path.realpath(__file__))
			arch_path = script_dir + "/../architectures/s10.json"
			device_path = script_dir + "/../devices/s10_2800.json"
			cls._current_arch = cls(arch_path, device_path)
		return cls._current_arch

	def __init__(self, arch_path, device_path):
		super(Architecture, self).__init__()
		Architecture.current_arch = self
		with open(arch_path, "r") as fh:
			self.arch_spec = json.load(fh)
		with open(device_path, "r") as fh:
			self.device_spec = json.load(fh)
		self.memory_map_order, self.count_for_ram_type = self._memory_map_order()

	def _memory_map_order(self):
		count_for_ram_type = []
		l = []
		ram_type = 0
		max_ram_cost = min([c for c in self.device_spec["RAMs"].values()])
		for ram_type, (k,v) in enumerate(self.arch_spec["RAMs"].items()):
			cost = max_ram_cost / self.device_spec["RAMs"][k]
			block_spec = [s + [ram_type, cost] for s in v["shapes"]]
			l += block_spec
			count_for_ram_type.append(self.device_spec["RAMs"][k])


		# prioritize deepest shapes first, but stop using deep
		# RAMs when we underutilize them
		def get_cost(e):
			return - e[1]**2 * e[3] * e[0]
		l.sort(key=get_cost)
		return l, count_for_ram_type

	@classmethod
	def ram_depth_cliffs(cls):
		self = Architecture.current_arch
		cliffs = []
		previous_memory_spec = self.memory_map_order[0]
		for memory_spec in self.memory_map_order[1:]:
			if memory_spec[2] != previous_memory_spec[2]:
				acceptable_number = previous_memory_spec[3] // memory_spec[3]
				acceptable_depth = (memory_spec[1] * acceptable_number) // (previous_memory_spec[0] // memory_spec[0])
				cliffs.append(acceptable_depth)

		return cliffs


	@classmethod
	def estimate_ram_counts(cls, dim_x, dim_y):
		self = Architecture.current_arch

		def cost_to_cover(ram_idx, remaining_y):
			xcost = math.ceil(dim_x / self.memory_map_order[ram_idx][0])
			ycost = remaining_y // self.memory_map_order[ram_idx][1]
			remainder_y = remaining_y % self.memory_map_order[ram_idx][1]
			count = xcost * ycost
			cost_to_cover = count * self.memory_map_order[ram_idx][-1]
			return count, cost_to_cover, remainder_y, xcost, xcost * self.memory_map_order[ram_idx][-1]

		covered = False
		remaining_y = dim_y
		current_idx = 0
		count, c, remaining_y, x, xc = cost_to_cover(0, remaining_y)
		counts = [0 for _ in range(len(self.memory_map_order))]
		for i in range(1, len(self.memory_map_order)):
			counts[i-1] = count
			count, c, remaining_y, next_x, next_xc = cost_to_cover(i, remaining_y)
			# no cheaper to use this ram than the last one
			if c >= xc:
				counts[i-1] += x
				break
			x = next_x
			xc = next_xc

		# reduce to counts for each ram type
		count_by_ram_type = [0 for _ in self.count_for_ram_type]
		for count, ram_type in zip(counts, self.memory_map_order):
			count_by_ram_type[ram_type[2]] += count

		return count_by_ram_type

