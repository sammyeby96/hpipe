import os.path
import sys
from gen_pure_verilog.utils import get_lines_in_file
    

def main(argv):
    path = argv[1]
    op_names = get_lines_in_file(path + "/ops.txt")
    allocated = ["*", "_", "_", "_", "_", "_", "_", "_"]
    def print_allocated():
        nonlocal allocated
        print(" ".join(allocated))
    def update_allocated(path):
        nonlocal allocated
        for i,a in enumerate(allocated):
            if a != "_":
                allocated[i] = "*"

        ops = get_lines_in_file(path + "/layer_names.txt")
        for op in ops:
            input_index_path = path + "/" + op + "/input_index.txt"
            if os.path.exists(input_index_path):
                input_index = int(get_lines_in_file(input_index_path)[0])
                allocated[input_index] = "u"
        output_indices = get_lines_in_file(path + "/output_indices.txt")
        for i in output_indices:
            index = int(i)
            assert(allocated[index] != "u")
            allocated[index] = "+"

    for op_name in op_names:
        print_allocated()
        update_allocated(path + "/" + op_name)
    print_allocated()

if __name__ == "__main__":
    main(sys.argv)
